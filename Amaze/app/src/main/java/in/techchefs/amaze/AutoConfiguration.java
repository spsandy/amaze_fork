package in.techchefs.amaze;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.mobileconnectors.s3.transfermanager.PersistableTransfer;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.internal.S3ProgressListener;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.util.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import in.techchefs.amaze.controllers.RefreshData;

public class AutoConfiguration extends Activity  {
    private ListView mBucket_List_view             = null;
    private String[] mBucketsNames                 = null;
    private TextView mPermission_message           = null;
    private String  mSelectedBucketName            = "NoBucket";
    private TransferManager mTransferManager       = null;
    private Boolean mIsBillingAvalible             = false;
    private String mCsvLocation                    = "";
    private Boolean mIsPreveousDownload            = false;
    private AmazonS3Client mAmazonS3Client         = null;
    private BasicAWSCredentials mCredentials       = null;
    private String mValidateStatus                 = "NoError";
    public static List<Bucket> mListOfBuckets      = null;
    private ProgressDialog mProgressDialog         = null;
    private HashMap<String,List<String>> mBucketsInfo = null;
    public static String mInfoBucketName           = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_buckets);
        init();
        MonthReportDownLoad();
        if(mIsPreveousDownload){
            NavigateToDashBoard();
        }else{
            SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate", MODE_MULTI_PROCESS);
            String lAccess_key = lSharedPreferences.getString("accesskey", "error");
            String lSecret_key = lSharedPreferences.getString("secretkey", "error");
            mCredentials         = new BasicAWSCredentials(lAccess_key, lSecret_key);
            mAmazonS3Client      = new AmazonS3Client(mCredentials);
            mTransferManager     = new TransferManager(mCredentials);
            AccessBucket accessBucket            = new AccessBucket();
            accessBucket.execute();
            ShowProgressDialog(true,"Fetching Bills Please Wait....");
        }
    }
   private void MonthReportDownLoad() {
          mCsvLocation  = getFilesDir()+"/";
            File lFile    = new File(mCsvLocation);
            File lListOfFiles []= lFile.listFiles();
            if(lListOfFiles.length > 0){
                mIsPreveousDownload = true;
            }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_buckets, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            ShowToast("Help");
            return true;
        }
        if (id == R.id.action_logout) {
            ShowToast("Logout");
            return true;
        }
        if (id == R.id.action_configuration) {
            ShowToast("Configuration");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /*private void SetBucketsListView() {
       mBucketsNames =new String[mListOfBuckets.size()];
        for (int i = 0; i<mListOfBuckets.size();i++){
            mBucketsNames[i] = mListOfBuckets.get(i).getName().toString();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mBucketsNames);
        //Assign adapter to ListView
        mBucket_List_view.setAdapter(adapter);
        mBucket_List_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String lBucketname = ((TextView) view).getText().toString();
                //ShowToast(lBucketname);
                SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate", MODE_MULTI_PROCESS);
                String lAccess_key = lSharedPreferences.getString("accesskey", "error");
                String lSecret_key = lSharedPreferences.getString("secretkey", "error");
                mCredentials = new BasicAWSCredentials(lAccess_key, lSecret_key);
                mAmazonS3Client = new AmazonS3Client(mCredentials);
                mSelectedBucketName = lBucketname;
                mTransferManager = new TransferManager(mCredentials);
                mIsBillingAvalible = false;
                ShowProgressDialog(true);
                FilesInsideBucket filesInsideBucket = new FilesInsideBucket();
                filesInsideBucket.execute();
            }
        });
   }*/
    private void ShowToast(String text) {
        Toast.makeText(AutoConfiguration.this,text,Toast.LENGTH_LONG).show();
    }
    private void init() {
        //mBucket_List_view = (ListView) findViewById(R.id.bucket_list_view);

    }
    private void NavigateToDashBoard() {
        Intent lIntent_dashboard = new Intent(AutoConfiguration.this,DashBoard.class);
        startActivity(lIntent_dashboard);
        finish();
    }
    private void ShowProgressDialog(boolean isshow,String ltext) {
        if(isshow){
            mProgressDialog = new ProgressDialog(AutoConfiguration.this);
            mProgressDialog.setMessage(ltext);
            mProgressDialog.setCancelable(false);
            if(!mProgressDialog.isShowing())
            mProgressDialog.show();
        }else{
            if(mProgressDialog!=null) {
                if(mProgressDialog.isShowing()){
                     mProgressDialog.cancel();
                     mProgressDialog = null;
                }
            }
        }
    }
    class FilesInsideBucket extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
            Calendar cal = Calendar.getInstance();
            int lYear  = cal.get(Calendar.YEAR);
            int lMonth = cal.get(Calendar.MONTH)+1;
            String lDate = "";
            if(lMonth>9){
                lDate = lYear+"-"+lMonth;
            }
            else
            {
                lDate = lYear+"-0"+lMonth;
            }
            String lCurrentMonthFile = ""+lDate+".csv";
                for (int i = 0; i<mListOfBuckets.size();i++){
                    ObjectListing lList = null;
                    try {
                        lList = mAmazonS3Client.listObjects(mListOfBuckets.get(i).getName(), "");
                    }catch (AmazonS3Exception e){

                    }
                if(lList!=null) {
                    List<S3ObjectSummary> lListObjectSummaries = lList.getObjectSummaries();
                    for (S3ObjectSummary s3ObjectSummary : lListObjectSummaries)
                    {
                        if ((s3ObjectSummary.getKey().toString().contains("-aws-billing-csv-"))) {
                            //TransferController.download(DisplayBuckets.this,new String[]{"118558798780-aws-billing-csv-2015-05.csv"});
                            //Log.i("-aws-billing-csv-","s3ObjectSummary.getKey().toString();");
                            mInfoBucketName = mListOfBuckets.get(i).getName();
                            if(s3ObjectSummary.getKey().toString().contains( lCurrentMonthFile)){
                                mIsBillingAvalible = true;
                                SharedPreferences BucketConfig = getSharedPreferences("BucketConfig", MODE_MULTI_PROCESS);
                                SharedPreferences.Editor value = BucketConfig.edit();
                                value.putString("bucketname", mInfoBucketName);
                                value.putString("filename", s3ObjectSummary.getKey().toString());
                                value.commit();
                                Intent intent = new Intent(AutoConfiguration.this, RefreshData.class);
                                PendingIntent pendingIntent = PendingIntent.getBroadcast(AutoConfiguration.this, 0, intent, 0);
                                AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                                int interval = 1000 * 60 * 180;
                                manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+1000 * 60 * 20
                                        , interval, pendingIntent);
                                break;
                            }
                        }
                    }
                    if(mIsBillingAvalible){

                        List <String>lListOFCsvs = new ArrayList<String>();
                        List<S3ObjectSummary> llListObjectSummaries = lList.getObjectSummaries();
                        for (S3ObjectSummary s3ObjectSummary : llListObjectSummaries) {
                            if ((s3ObjectSummary.getKey().toString().contains("-aws-billing-csv-"))) {
                                lListOFCsvs.add(s3ObjectSummary.getKey().toString());
                            }
                        }
                        int lMaxFile = 6;
                        if(lListOFCsvs.size() > 6){
                            lMaxFile = 6;
                        }
                        else {
                            lMaxFile = lListOFCsvs.size();
                        }
                         int temp = lListOFCsvs.size() ;
                         for (int filenameindex = temp - lMaxFile;filenameindex < lListOFCsvs.size() ; filenameindex++ )
                          {
                            if ((lListOFCsvs.get(filenameindex).contains("-aws-billing-csv-")))
                            {
                                FileOutputStream outputStream ;
                                File file = new File(getFilesDir().getAbsolutePath() + "/" + lListOFCsvs.get(filenameindex));
                            try
                            {
                                if (!file.exists()){
                                    file.createNewFile();
                                    file.setWritable(true);
                                }
                                outputStream                      = new FileOutputStream(file);
                                GetObjectRequest getObjectRequest = new GetObjectRequest(mListOfBuckets.get(i).getName(), lListOFCsvs.get(filenameindex));
                                S3Object object                   = mAmazonS3Client.getObject((getObjectRequest));
                                InputStream objectData = object.getObjectContent();
                                byte[] buffer = new byte[1024];
                                int read = 0;
                                //objectData.
                                // IOUtils.copy(objectData, outputStream);
                                while ((read = objectData.read(buffer)) != -1) {
                                    outputStream.write(buffer, 0, read);
                                }
                                objectData.close();
                                outputStream.close();

                            }catch (IOException e) {
                                     e.printStackTrace();
                            }finally {

                            }
                            }
                   }
                        break;
                    }
                    }
                }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
               ShowProgressDialog(false,"");
            if(mIsBillingAvalible) {
                NavigateToDashBoard();
            }
            else{
                Intent lIntent_creat_bucket = new Intent(AutoConfiguration.this,CreateBucket.class);
                startActivity(lIntent_creat_bucket);
                finish();
            }
            super.onPostExecute(aVoid);
        }
    }
    private void BucketOprations() {
        if(mListOfBuckets.size()==0){
            Intent lIntent_creat_bucket = new Intent(AutoConfiguration.this,CreateBucket.class);
            startActivity(lIntent_creat_bucket);
            finish();
        }else{
           if(mIsPreveousDownload){
               Intent lIntent_dashboard = new Intent(AutoConfiguration.this,DashBoard.class);
               startActivity(lIntent_dashboard);
               finish();
           }else {
               mIsBillingAvalible   = false;
               FilesInsideBucket filesInsideBucket = new FilesInsideBucket();
               filesInsideBucket.execute();
               ShowProgressDialog(true,"Fetching Bills Please Wait....");
           }
        }
    }
    class AccessBucket extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                mAmazonS3Client.getS3AccountOwner();
                mListOfBuckets  =  mAmazonS3Client.listBuckets();
            }catch (AmazonS3Exception s3) {
                Log.i("bucket ",s3.getErrorCode());
                mValidateStatus = s3.getErrorCode();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(mValidateStatus.equals("AccessDenied")){
                ShowProgressDialog(false,"");
                SharedPreferences Loginstate    = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
                SharedPreferences.Editor value  = Loginstate.edit();
                value.putBoolean("LoginState", false);
                value.commit();
                Intent lIntent_login = new Intent(AutoConfiguration.this,LoginActivity.class);
                startActivity(lIntent_login);
                finish();
            }else{
                BucketOprations();
            }
            super.onPostExecute(aVoid);
        }
    }

}
