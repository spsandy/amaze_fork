package in.techchefs.amaze.extras;

import java.util.Comparator;

import in.techchefs.amaze.modles.ParserModel;

/**
 * Created by sandeep on 8/6/15.
 */
public class BillingListSortBasedOnProductName implements Comparator<ParserModel> {
    @Override
    public int compare(ParserModel lhs, ParserModel rhs) {
        return lhs.getmProductName().compareToIgnoreCase(rhs.getmProductName());
    }

}
