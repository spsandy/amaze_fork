package in.techchefs.amaze.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import au.com.bytecode.opencsv.CSVReader;
import in.techchefs.amaze.BillDetailBasedOnUsageType;
import in.techchefs.amaze.R;
import in.techchefs.amaze.modles.AWSServices;
import in.techchefs.amaze.modles.ParserModel;

/**
 * Created by sandeep on 12/6/15.
 */
public class DashBoardListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<AWSServices> mListOfServices = null;

    public DashBoardListAdapter(Context context, List lService) {
        this.mListOfServices = lService;
        this.context = context;
    }

    @Override
    public int getCount() {
        return mListOfServices.size()>=5? 5:mListOfServices.size();
    }

    @Override
    public Object getItem(int position) {
        return mListOfServices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            /*convertView = inflater.inflate(R.layout.list_dashboard, null);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView cost = (TextView) convertView.findViewById(R.id.cost);*/
            convertView = inflater.inflate(R.layout.list_billdetail, null);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView cost = (TextView) convertView.findViewById(R.id.cost);
            ImageView icon = (ImageView) convertView.findViewById(R.id.arraow);
            name.setText("  " + mListOfServices.get(position).getmNAme());
            Double d = Double.parseDouble(mListOfServices.get(position).getmTotalCost());
            final String ltotal = String.format("%.2f", d);
            cost.setText("$" + ltotal);

            if(mListOfServices.get(position).getmNAme().equals("S3")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_s3));
                icon.setImageResource(R.drawable.ic_s3);
            }
            else if(mListOfServices.get(position).getmNAme().equals("IAM")){
                //icon.setBackground(mContext.getDrawable(R.drawable.ic_iam));
                icon.setImageResource(R.drawable.ic_iam);
            }
            else if(mListOfServices.get(position).getmNAme().equals("EBS")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_ebs));
                icon.setImageResource(R.drawable.ic_ebs);
            }
            else if(mListOfServices.get(position).getmNAme().equals("EFS")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_efs));

                icon.setImageResource(R.drawable.ic_efs);
            }
            else if(mListOfServices.get(position).getmNAme().equals("CloudWatch")){
                //  icon.setBackground(mContext.getDrawable(R.drawable.ic_cloudwatch));
                icon.setImageResource(R.drawable.ic_cloudwatch);
            }
            else if(mListOfServices.get(position).getmNAme().equals("CloudFront")){
                //  icon.setBackground(mContext.getDrawable(R.drawable.ic_cloudfront));
                icon.setImageResource(R.drawable.ic_cloudfront);
            }
            else if(mListOfServices.get(position).getmNAme().equals("EC2")){
                //  icon.setBackground(mContext.getDrawable(R.drawable.ic_ec2));
                icon.setImageResource(R.drawable.ic_ec2);
            }
            else if(mListOfServices.get(position).getmNAme().equals("SNS")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_sns));
                icon.setImageResource(R.drawable.ic_sns);
            }
            else if(mListOfServices.get(position).getmNAme().equals("Mobile Analytics")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_mobileanlytics));
                icon.setImageResource(R.drawable.ic_mobileanlytics);
            }
            else if(mListOfServices.get(position).getmNAme().equals("Route 53")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_mobileanlytics));
                icon.setImageResource(R.drawable.ic_mobileanlytics);
            }
            else if(mListOfServices.get(position).getmNAme().equals("EMR")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_emr));
                icon.setImageResource(R.drawable.ic_emr);
            }
            else if(mListOfServices.get(position).getmNAme().equals("SQS")){
                //  icon.setBackground(mContext.getDrawable(R.drawable.ic_sqs));
                icon.setImageResource(R.drawable.ic_sqs);
            }
            else if(mListOfServices.get(position).getmNAme().equals("Storage Gateway")){
                // icon.setBackground(mContext.getDrawable(R.drawable.ic_storagegateway));
                icon.setImageResource(R.drawable.ic_storagegateway);
            }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BillDetailBasedOnUsageType.class);
                    intent.putExtra("Total", ltotal.toString());
                    intent.putExtra("name", mListOfServices.get(position).getmNAme());
                    context.startActivity(intent);
                }
            });
        }

        return convertView;
    }
}