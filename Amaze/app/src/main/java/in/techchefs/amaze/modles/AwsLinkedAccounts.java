package in.techchefs.amaze.modles;

/**
 * Created by sandeep on 15/6/15.
 */
public class AwsLinkedAccounts {
    private String mAccountOwnerName = null;
    private String mTotalCost        = null;

    public void setmTotalCost(String mTotalCost) {
        this.mTotalCost = mTotalCost;
    }

    public void setmAccountOwnerName(String mAccountOwnerName) {
        this.mAccountOwnerName = mAccountOwnerName;
    }

    public String getmAccountOwnerName() {
        return mAccountOwnerName;
    }

    public String getmTotalCost() {
        return mTotalCost;
    }
}
