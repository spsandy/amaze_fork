package in.techchefs.amaze;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;

import in.techchefs.amaze.adapter.BillAdapter;
import in.techchefs.amaze.modles.UsageDecription;


public class BillDetail extends ActionBarActivity   {
    private Toolbar mToolbar                                = null;
    private ListView mListViewOfServices                    = null;
    private SearchView searchView                           = null;
    private MenuItem searchItem                             = null;
    SearchView.SearchAutoComplete    mTheTextArea           = null;
    private HashMap<String,HashMap<String,UsageDecription>> mReoptsByUsageType           = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        init();
        setListView();
    }
    private void setListView() {
        final BillAdapter lBillAdapter = new BillAdapter(DashBoard.mListOfServices,BillDetail.this);
        mListViewOfServices.setAdapter(lBillAdapter);
        mListViewOfServices.setTextFilterEnabled(true);
        String lHints[] = new String[]{
                "Mobile Analytics",
                "SNS",
                "S3",
                "EFS",
                "Storage Gateway",
                "Glacier",
                "CloudFront",
                "EBS",
                "Directory Services",
                "IAM",
                "CloudTrail",
                "Config",
                "CloudWatch",
                "KMS",
                "CloudHSM",
                "EC2",
                "EC2 Container",
                "Auto Scaling",
                "Load Balancing",
                "Lambda",
                "VPC",
                "Route 53",
                "Direct Connect",
                "WorkSpaces",
                "WAM",
                "WorkDocs",
                "WorkMail",
                "EMR",
                "Kinesis",
                "Data Pipeline",
                "Machine Learning"
        };
        mTheTextArea.setAdapter(new ArrayAdapter<String>(BillDetail.this, R.layout.li_query_suggestion_services, lHints));;

        mTheTextArea.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) view.findViewById(R.id.searchid);
                mTheTextArea.setText(tv.getText());
                String text = mTheTextArea.getText().toString().toLowerCase(Locale.getDefault());

            }

        });
        mTheTextArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lBillAdapter.filter(mTheTextArea.getText().toString().toLowerCase(Locale.getDefault()));

            }
        });

    }
    private void init() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.inflateMenu(R.menu.menu_bill_detail_based_on_usage_type);

        // mToolbar.setNavigationIcon(R.drawable.menubutton);
        searchItem                                  =  mToolbar.getMenu().findItem(R.id.action_search);
        searchView                                  =  (SearchView) MenuItemCompat.getActionView(searchItem);
        mTheTextArea                                = (SearchView.SearchAutoComplete)searchView.findViewById(R.id.search_src_text);
        mListViewOfServices = (ListView)findViewById(R.id.listofservices);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.action_change_bucket_usage) {
                    if (isNetworkAvaliable(BillDetail.this)) {
                        for (File file : getFilesDir().listFiles())
                            file.delete();
                        Intent intent = new Intent(BillDetail.this, AutoConfiguration.class);
                        startActivity(intent);
                        finish();
                        return true;
                    } else {
                        final Dialog dialog = new Dialog(BillDetail.this);
                        dialog.setContentView(R.layout.custom);
                        dialog.setTitle("");
                        TextView text = (TextView) dialog.findViewById(R.id.text);
                        text.setText("The Internet connection appears to be offline Please Check Your Internet Settings");
                        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });
                        dialog.show();
                    }

                }
                if (id == R.id.action_logout_usage) {
                    for (File file : getFilesDir().listFiles())
                        file.delete();
                    SharedPreferences Loginstate = getSharedPreferences("Loginstate", MODE_MULTI_PROCESS);
                    SharedPreferences.Editor value = Loginstate.edit();
                    value.putBoolean("LoginState", false);
                    value.commit();
                    Intent intent = new Intent(BillDetail.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });

    }
    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }
}
