package in.techchefs.amaze.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import in.techchefs.amaze.R;
import in.techchefs.amaze.modles.AwsLinkedAccounts;

/**
 * Created by sandeep on 13/6/15.
 */
public class DrawerAdapter extends ArrayAdapter<String> {
    Context mcoContext = null;
    List<String >mListOfDrawer= null;
    String mAdminName = null;
    public DrawerAdapter(Context context, String mAdminName,List<String> mListOfDrawer) {
        super(context, -1, mListOfDrawer);
        mcoContext =context;
        this.mListOfDrawer = mListOfDrawer;
        this.mAdminName = mAdminName;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) mcoContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.drawer_hader, parent, false);
            TextView textView = (TextView) rowView.findViewById(R.id.textView2);
        if(mListOfDrawer.get(position).equalsIgnoreCase("View\n" +
                "Billing\n" +
                "\n" +
                "My Account")){
            textView.setBackgroundColor(Color.parseColor("#f15a29"));
            textView.setTextSize(23);
            textView.setTextColor(Color.parseColor("#FFFFFF"));
        }
        else if(mListOfDrawer.get(position).equalsIgnoreCase("Admin")){
            textView.setTextColor(Color.parseColor("#f15a29"));
        }
        else if(mListOfDrawer.get(position).equalsIgnoreCase(mAdminName)&&(position == 2)){
           //textView.setTextColor(Color.parseColor("#f15a29"));
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.admin, 0, 0, 0);
        }
        else if(mListOfDrawer.get(position).equalsIgnoreCase("LinkedAccounts")){
            textView.setTextColor(Color.parseColor("#f15a29"));
        }else {
            textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.linkedaccounts, 0, 0, 0);
        }
            textView.setText(mListOfDrawer.get(position));
        return rowView;
        }
}