package in.techchefs.amaze;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class AmazeSplash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amaze_splash);

            Thread background = new Thread() {
                public void run() {

                    try {
                        sleep(2 * 1000);
                        Intent i = new Intent(AmazeSplash.this, LoginActivity.class);
                        startActivity(i);
                        finish();

                    } catch (Exception e) {

                    }
                }
            };

            // start thread
            background.start();

        }

}
