package in.techchefs.amaze.modles;
import java.util.List;

public class ParserModel {
    private String mProductName             = null;
    private String mUsageType               = null;
    private String mItemDescription         = null;
    private String mUsageQuantity           = null;
    private String mTotalCost               = null;
    private String mPayerAccountName        = null;
    private String mLinkedAccountName       = null;
    private String mBillingPeriodEndDate    = null;
    private String mInvoiceDate             = null;
    private String mCurrencyCode            = null;
    private String mTaxationAddress         = null;
    private String mSellerOfRecord          = null;
    private String mPayerAccountId          = null;
    public void setmPayerAccountId(String mPayerAccountId) {
        this.mPayerAccountId = mPayerAccountId;
    }
    public String getmPayerAccountId() {
        return mPayerAccountId;
    }
    private String mBillingPeriodStartDate = null;

    public String getmBillingPeriodStartDate() {
        return mBillingPeriodStartDate;
    }

    public String getmBillingPeriodEndDate() {
        return mBillingPeriodEndDate;
    }

    public String getmInvoiceDate() {
        return mInvoiceDate;
    }

    public String getmCurrencyCode() {
        return mCurrencyCode;
    }

    public String getmTaxationAddress() {
        return mTaxationAddress;
    }

    public String getmSellerOfRecord() {
        return mSellerOfRecord;
    }

    public void setmBillingPeriodStartDate(String mBillingPeriodStartDate) {
        this.mBillingPeriodStartDate = mBillingPeriodStartDate;
    }

    public void setmBillingPeriodEndDate(String mBillingPeriodEndDate) {
        this.mBillingPeriodEndDate = mBillingPeriodEndDate;
    }

    public void setmInvoiceDate(String mInvoiceDate) {
        this.mInvoiceDate = mInvoiceDate;
    }

    public void setmCurrencyCode(String mCurrencyCode) {
        this.mCurrencyCode = mCurrencyCode;
    }

    public void setmTaxationAddress(String mTaxationAddress) {
        this.mTaxationAddress = mTaxationAddress;
    }

    public void setmSellerOfRecord(String mSellerOfRecord) {
        this.mSellerOfRecord = mSellerOfRecord;
    }

    public void setmPayerAccountName(String mPayerAccountName) {
        this.mPayerAccountName = mPayerAccountName;
    }

    public void setmLinkedAccountName(String mLinkedAccountName) {
        this.mLinkedAccountName = mLinkedAccountName;
    }

    public String getmPayerAccountName() {
        return mPayerAccountName;
    }

    public String getmLinkedAccountName() {
        return mLinkedAccountName;
    }

    public String getmProductName() {
        return mProductName;
    }

    public String getmUsageType() {
        return mUsageType;
    }

    public String getmItemDescription() {
        return mItemDescription;
    }

    public String getmUsageQuantity() {
        return mUsageQuantity;
    }

    public void setmProductName(String mProductName) {
        this.mProductName = mProductName;
    }

    public void setmUsageType(String mUsageType) {
        this.mUsageType = mUsageType;
    }

    public void setmItemDescription(String mItemDescription) {
        this.mItemDescription = mItemDescription;
    }

    public void setmUsageQuantity(String mUsageQuantity) {
        this.mUsageQuantity = mUsageQuantity;
    }

    public void setmTotalCost(String mTotalCost) {
        this.mTotalCost = mTotalCost;
    }

    public String getmTotalCost() {
        return mTotalCost;

    }

    public  List<ParserModel> GetListOfproducts(List<ParserModel> lParserModelList){


        return lParserModelList;
    }
    public  List<ParserModel> GetListOfLinkedAccount(List<ParserModel> lParserModelList){


        return lParserModelList;
    }

}
