package in.techchefs.amaze.extras;

import java.util.Comparator;

import in.techchefs.amaze.modles.ParserModel;

/**
 * Created by sandeep on 18/6/15.
 */
public class BillingListSortBasedOnTotalBill implements Comparator<ParserModel> {
    @Override
    public int compare(ParserModel lhs, ParserModel rhs) {
        return Double.compare(Double.parseDouble(lhs.getmTotalCost()),Double.parseDouble(rhs.getmTotalCost()));
    }
}
