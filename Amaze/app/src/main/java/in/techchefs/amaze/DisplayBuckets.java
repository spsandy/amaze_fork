package in.techchefs.amaze;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class DisplayBuckets extends Activity {
    private ListView mBucket_List_view             = null;
    private String[] mBucketsNames                 = null;
    private TextView mPermission_message           = null;

    private String  mSelectedBucketName            = "NoBucket";
    private TransferManager mTransferManager       = null;
    private Boolean mIsBillingAvalible             = false;
    private String mCsvLocation                    = "";
    private Boolean mIsPreveousDownload            = false;
    private AmazonS3Client mAmazonS3Client          = null;
    private BasicAWSCredentials mCredentials       = null;
    private String mValidateStatus                 = "NoError";
    public static List<Bucket> mListOfBuckets      = null;
    private ProgressDialog mProgressDialog         = null;
    private HashMap<String,List<String>> mBucketsInfo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_buckets);
        init();
        MonthReportDownLoad();
        if(mIsPreveousDownload){
            NavigateToDashBoard();
        }else{
            SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate", MODE_MULTI_PROCESS);
            String lAccess_key = lSharedPreferences.getString("accesskey", "error");
            String lSecret_key = lSharedPreferences.getString("secretkey", "error");
            mCredentials         = new BasicAWSCredentials(lAccess_key, lSecret_key);
            mAmazonS3Client      = new AmazonS3Client(mCredentials);
            mTransferManager     = new TransferManager(mCredentials);
            AccessBucket accessBucket            = new AccessBucket();
            accessBucket.execute();
            ShowProgressDialog(true);
        }
    }

    private void MonthReportDownLoad() {

            mCsvLocation  = getFilesDir()+"/";

            File lFile    = new File(mCsvLocation);

            File lListOfFiles []= lFile.listFiles();
            //118558798780-aws-billing-csv-2015-05.csv
            if(lListOfFiles.length > 1){
                mIsPreveousDownload = true;
            }
       /* FilesInsideBucket lFilesInsideBucket = new FilesInsideBucket();
        lFilesInsideBucket.execute();*/


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_buckets, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            ShowToast("Help");
            return true;
        }
        if (id == R.id.action_logout) {
            ShowToast("Logout");
            return true;
        }
        if (id == R.id.action_configuration) {
            ShowToast("Configuration");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void SetBucketsListView() {
       mBucketsNames =new String[mListOfBuckets.size()];
        for (int i = 0; i<mListOfBuckets.size();i++){
            mBucketsNames[i] = mListOfBuckets.get(i).getName().toString();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mBucketsNames);
        //Assign adapter to ListView
        mBucket_List_view.setAdapter(adapter);
        mBucket_List_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String lBucketname = ((TextView) view).getText().toString();
                //ShowToast(lBucketname);
                SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate", MODE_MULTI_PROCESS);
                String lAccess_key = lSharedPreferences.getString("accesskey", "error");
                String lSecret_key = lSharedPreferences.getString("secretkey", "error");
                mCredentials = new BasicAWSCredentials(lAccess_key, lSecret_key);
                mAmazonS3Client = new AmazonS3Client(mCredentials);
                mSelectedBucketName = lBucketname;
                mTransferManager = new TransferManager(mCredentials);
                mIsBillingAvalible = false;
                ShowProgressDialog(true);
                FilesInsideBucket filesInsideBucket = new FilesInsideBucket();
                filesInsideBucket.execute();
            }
        });
   }
    private void ShowToast(String text) {
        Toast.makeText(DisplayBuckets.this,text,Toast.LENGTH_LONG).show();
    }
    private void init() {
        //mBucket_List_view = (ListView) findViewById(R.id.bucket_list_view);

    }
    private void NavigateToDashBoard() {
        Intent lIntent_dashboard = new Intent(DisplayBuckets.this,DashBoard.class);
        startActivity(lIntent_dashboard);
        finish();
    }
    private void ShowProgressDialog(boolean isshow) {
        if(isshow){
            mProgressDialog = new ProgressDialog(DisplayBuckets.this);
            mProgressDialog.setMessage("please wait....");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }else{
            if(mProgressDialog!=null) {
                mProgressDialog.cancel();
                mProgressDialog = null;
            }
        }
    }
    class FilesInsideBucket extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {

                for (int i = 0; i<mListOfBuckets.size();i++){
                ObjectListing lList = mAmazonS3Client.listObjects(mListOfBuckets.get(i).getName(), "");
                if(lList!=null){
                List<S3ObjectSummary> lListObjectSummaries = lList.getObjectSummaries();
                for(S3ObjectSummary s3ObjectSummary:lListObjectSummaries){
                    if((s3ObjectSummary.getKey().toString().endsWith(".csv"))&&(s3ObjectSummary.getKey().toString().contains("-aws-billing-"))){
                      //  TransferController.download(DisplayBuckets.this,new String[]{"118558798780-aws-billing-csv-2015-05.csv"});
                        mIsBillingAvalible = true;
                        File file = new File(getFilesDir().getAbsolutePath()+"/"+s3ObjectSummary.getKey().toString());
                        try {
                            if(!file.exists()) {
                                file.createNewFile();
                                file.setWritable(true);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                       /* Calendar cal = Calendar.getInstance();
                        int lYear  = cal.get(Calendar.YEAR);
                        int lMonth = cal.get(Calendar.MONTH);
                        String lDate = "";
                        if(lMonth>9){
                             lDate = lYear+"-"+lMonth;
                        }
                        else
                        {
                            lDate = lYear+"-0"+lMonth;
                        }
*/
                        try {
                            mTransferManager.download(mListOfBuckets.get(i).getName(), s3ObjectSummary.getKey().toString(), file);
                        }catch (Exception e) {

                        }
                    }
                }
             }
           }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
               ShowProgressDialog(false);
               NavigateToDashBoard();
            super.onPostExecute(aVoid);
        }
    }
    private void BucketOprations() {
        if(mListOfBuckets.size()==0){
            Intent lIntent_creat_bucket = new Intent(DisplayBuckets.this,CreateBucket.class);
            startActivity(lIntent_creat_bucket);
            finish();
        }else{
           if(mIsPreveousDownload){
               Intent lIntent_dashboard = new Intent(DisplayBuckets.this,DashBoard.class);
               startActivity(lIntent_dashboard);
               finish();
           }else {

               mIsBillingAvalible   = false;
               FilesInsideBucket filesInsideBucket = new FilesInsideBucket();
               filesInsideBucket.execute();
               ShowProgressDialog(true);
           }
        }
    }
    class AccessBucket extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                mAmazonS3Client.getS3AccountOwner();
                mListOfBuckets  =  mAmazonS3Client.listBuckets();
            }catch (AmazonS3Exception s3) {
                Log.i("bucket ",s3.getErrorCode());
                mValidateStatus = s3.getErrorCode();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(mValidateStatus.equals("AccessDenied")){
                ShowProgressDialog(false);
                SharedPreferences Loginstate    = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
                SharedPreferences.Editor value  = Loginstate.edit();
                value.putBoolean("LoginState", false);
                value.commit();
                Intent lIntent_login = new Intent(DisplayBuckets.this,LoginActivity.class);
                startActivity(lIntent_login);
                finish();
            }else{
                BucketOprations();
            }
            super.onPostExecute(aVoid);
        }
    }

}
