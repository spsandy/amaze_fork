package in.techchefs.amaze;;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import in.techchefs.amaze.adapter.BillAdapterUsageType;
import in.techchefs.amaze.modles.ParserModel;
import in.techchefs.amaze.modles.UsageDecription;

public class BillDetailBasedOnUsageType extends ActionBarActivity {
    private Toolbar  mToolbar             = null;
    private String   mServiceName         = null;
    private String   mTotalCost           = null;
    private ListView mListViewUsageType   = null;
    private ScrollView mScrollView        = null;
    List<String>       mListOfUsageType   = null;
    public static String     mPayerName        = null;
    public static String     mInvoiceDate      = null;
    public static String     mStratDate        = null;
    public static String     mEndDate          = null;
    public static String     mCurrency         = null;
    private MenuItem   mLogOut           = null;
    private MenuItem   mChangeBucket     = null;
    private SearchView searchView                           = null;
    private MenuItem searchItem                             = null;
    SearchView.SearchAutoComplete    mTheTextArea     = null;
    private HashMap<String,HashMap<String,UsageDecription>> mReoptsByUsageType           = null;
    private HashMap<String,UsageDecription>mCurrentDisplayList                           = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail_based_on_usage_type);
        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();
        init();
        GetIntentExtras(getIntent());
        GetServiceInfo();
        SetListView();
    }
    private void SetUpMenu() {
        mLogOut.setOnMenuItemClickListener(new MenuItemClick());
    }
    private void SetListView() {
        final BillAdapterUsageType lBillAdapterUsageType = new BillAdapterUsageType(BillDetailBasedOnUsageType.this,mCurrentDisplayList);
        mListViewUsageType.setAdapter(lBillAdapterUsageType);
        mListViewUsageType.setTextFilterEnabled(true);
        Utility.setListViewHeightBasedOnChildren(mListViewUsageType);
        String lHints[] = new String[mListOfUsageType.size()];
         for(int i =0;i<mListOfUsageType.size();i++){
            lHints[i] = mListOfUsageType.get(i);
        }
        mTheTextArea.setAdapter(new ArrayAdapter<String>(BillDetailBasedOnUsageType.this, R.layout.li_query_suggestion_services, lHints));;
        mTheTextArea.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView tv = (TextView) view.findViewById(R.id.searchid);
                mTheTextArea.setText(tv.getText());
                String text = mTheTextArea.getText().toString().toLowerCase(Locale.getDefault());
               // lBillAdapterUsageType.filter(text);
            }

        });
        mTheTextArea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                lBillAdapterUsageType.filter(mTheTextArea.getText().toString().toLowerCase(Locale.getDefault()));

            }
        });
    }
    private void GetServiceInfo() {
        mPayerName        = null;
        mInvoiceDate      = null;
        mStratDate        = null;
        mEndDate          = null;
        mCurrency         = null;
        List<ParserModel> lListOfParserModel =  DashBoard.mReportsOfAllMonths.get(DashBoard.mCurrentMonthFileName);
        for (ParserModel lParserModel : lListOfParserModel){
            String lProductName        = lParserModel.getmProductName();
            String lCurrentUsageType   = lParserModel.getmUsageType();
            Double lTotalAmt           = 0.0;
            try {
                lTotalAmt = Double.parseDouble(lParserModel.getmTotalCost());
            }catch (Exception e){

            }
            String lItemDiscription    = lParserModel.getmItemDescription();
            String lLinkedAccountName  = lParserModel.getmLinkedAccountName();
            if(lLinkedAccountName.trim().equals(DashBoard.mLinkedAccounName.trim())) {
                if (mPayerName == null&&(lParserModel.getmPayerAccountName()!= null)&&(lParserModel.getmPayerAccountName().length() > 0)) {
                    mPayerName = lParserModel.getmPayerAccountName();
                }
                if ((mInvoiceDate == null)&&(lParserModel.getmInvoiceDate()!= null)&&(lParserModel.getmInvoiceDate().length() > 0)) {
                    mInvoiceDate = lParserModel.getmInvoiceDate();
                   // Log.i("mInvoiceDate",mInvoiceDate) ;
                }
                if (mStratDate == null&&(lParserModel.getmBillingPeriodStartDate()!= null)&&(lParserModel.getmBillingPeriodStartDate().length() > 0)) {
                    mStratDate = lParserModel.getmBillingPeriodStartDate();
                   // Log.i("mStratDate",mStratDate) ;
                }
                if (mEndDate == null&&(lParserModel.getmBillingPeriodEndDate()!= null)&&(lParserModel.getmBillingPeriodEndDate().length() > 0)) {
                    mEndDate = lParserModel.getmBillingPeriodEndDate();
                    //Log.i("mEndDate",mEndDate) ;
                }
                if (mCurrency == null&&(lParserModel.getmCurrencyCode()!= null)&&(lParserModel.getmCurrencyCode().length() > 0)) {
                    mCurrency = lParserModel.getmCurrencyCode();
                   // Log.i("mCurrency",mCurrency) ;
                }
                Double lQuantity = 0.0;
                try {
                    lQuantity = Double.parseDouble(lParserModel.getmUsageQuantity());
                } catch (NumberFormatException e) {
                }
                HashMap<String, UsageDecription> lUsageDecriptionMap = mReoptsByUsageType.get(lProductName);
                if (lUsageDecriptionMap == null) {
                    HashMap<String, UsageDecription> lUsageMap = new HashMap<String, UsageDecription>();
                    UsageDecription lUsageDecription = new UsageDecription();
                    lUsageDecription.setDescription(lItemDiscription);
                    lUsageDecription.setmTotalCost(lTotalAmt);
                    lUsageDecription.setmQuatity(lQuantity);
                    lUsageDecription.setmUsageType(lCurrentUsageType);
                    lUsageMap.put(lCurrentUsageType, lUsageDecription);
                    mReoptsByUsageType.put(lProductName, lUsageMap);
                } else {
                    UsageDecription lUsageDecription = lUsageDecriptionMap.get(lCurrentUsageType);
                    if (lUsageDecription == null) {
                        UsageDecription llUsageDecription = new UsageDecription();
                        llUsageDecription.setDescription(lItemDiscription + "");
                        llUsageDecription.setmTotalCost(lTotalAmt);
                        llUsageDecription.setmLinkedAcconuName(lLinkedAccountName + "");
                        llUsageDecription.setmQuatity(lQuantity);
                        llUsageDecription.setmUsageType(lCurrentUsageType);
                        lUsageDecriptionMap.put(lCurrentUsageType, llUsageDecription);
                        //Log.i("first", "lTotalAmt = " + lTotalAmt + "  : lCurrentUsageType = " + lCurrentUsageType);
                    } else {
                        lUsageDecription.setmTotalCost((lUsageDecription.getmTotalCost() + lTotalAmt));
                        lUsageDecription.setmQuatity(lUsageDecription.getmQuatity() + lQuantity);
                    }
                }
            }
        }
        //Log.d("mServiceName",mServiceName);
        //Log.d("mCurrentMonthFileName",DashBoard.mCurrentMonthFileName);
        mCurrentDisplayList  =  mReoptsByUsageType.get(mServiceName);
        mListOfUsageType     = new ArrayList<String>();
        if(mCurrentDisplayList!=null){
            Iterator it = mCurrentDisplayList.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                UsageDecription lllUsageDecription = (UsageDecription) pair.getValue();
                mListOfUsageType.add(lllUsageDecription.getmUsageType());
                //mListOfUsageType.add(lllUsageDecription.getDescription());
            }
        }
    }
    private void GetIntentExtras(Intent intent) {
        mTotalCost   = intent.getStringExtra("Total");
        mServiceName = intent.getStringExtra("name");
        //Log.i("GetIntentExtras",mTotalCost + " : " +mServiceName);
    }
    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }
    private void init() {
        mReoptsByUsageType = new HashMap<String,HashMap<String,UsageDecription>>();
        mToolbar = (Toolbar) findViewById(R.id.toolbar_usage);
        mToolbar.inflateMenu(R.menu.menu_bill_detail_based_on_usage_type);
        searchItem                                  =  mToolbar.getMenu().findItem(R.id.action_search);
        searchView                                  =  (SearchView) MenuItemCompat.getActionView(searchItem);
        mTheTextArea                                = (SearchView.SearchAutoComplete)searchView.findViewById(R.id.search_src_text);
        mLogOut         = (MenuItem) mToolbar.findViewById(R.id.action_logout_usage);
        mChangeBucket   = (MenuItem)mToolbar.findViewById(R.id.action_change_bucket_usage);
        //List
        mListViewUsageType = (ListView)findViewById(R.id.listofservices_usage);
        //Search
        mScrollView   = (ScrollView) findViewById(R.id.scrollView_usage);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int id = menuItem.getItemId();
                if (id == R.id.action_change_bucket_usage) {
                    if(isNetworkAvaliable(BillDetailBasedOnUsageType.this)) {
                        for (File file : getFilesDir().listFiles())
                            file.delete();
                        Intent intent = new Intent(BillDetailBasedOnUsageType.this, AutoConfiguration.class);
                        startActivity(intent);
                        finish();
                        return true;
                    }else{
                        final Dialog dialog = new Dialog(BillDetailBasedOnUsageType.this);
                        dialog.setContentView(R.layout.custom);
                        dialog.setTitle("");
                        TextView text = (TextView) dialog.findViewById(R.id.text);
                        text.setText("The Internet connection appears to be offline Please Check Your Internet Settings");
                        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                        dialogButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();

                            }
                        });
                        dialog.show();
                    }
                }
                if (id == R.id.action_logout_usage) {
                    for(File file: getFilesDir().listFiles())
                        file.delete();
                    SharedPreferences Loginstate    = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
                    SharedPreferences.Editor value  = Loginstate.edit();
                    value.putBoolean("LoginState", false);
                    value.commit();
                    Intent intent = new Intent(BillDetailBasedOnUsageType.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                    return true;
                }
                return false;
            }
        });

    }
    static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }
            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
               // listItem.setBackgroundColor(Color.GREEN);
            }
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }
    class MenuItemClick  implements MenuItem.OnMenuItemClickListener{

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if(item.getItemId()==mLogOut.getItemId()){
                Toast.makeText(BillDetailBasedOnUsageType.this,"LOgOut",Toast.LENGTH_LONG).show();
                return true;
            }
            if(item.getItemId()==mChangeBucket.getItemId()){
                Toast.makeText(BillDetailBasedOnUsageType.this,"ChangeBucket",Toast.LENGTH_LONG).show();
                return true;
            }
            return false;
        }
    }
}
