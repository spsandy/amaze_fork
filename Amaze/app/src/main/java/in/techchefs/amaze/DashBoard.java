package in.techchefs.amaze;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.utils.PercentFormatter;
import com.suru.myp.MonthYearPicker;
import com.viewpagerindicator.CirclePageIndicator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import au.com.bytecode.opencsv.CSVReader;
import in.techchefs.amaze.adapter.DashBoardListAdapter;
import in.techchefs.amaze.adapter.DrawerAdapter;
import in.techchefs.amaze.extras.BillingListSortBasedOnLinkedAccountName;
import in.techchefs.amaze.extras.BillingListSortBasedOnPayerAccountName;
import in.techchefs.amaze.extras.BillingListSortBasedOnProductName;
import in.techchefs.amaze.modles.AWSServices;
import in.techchefs.amaze.modles.AwsLinkedAccounts;
import in.techchefs.amaze.modles.ParserModel;



public class DashBoard extends ActionBarActivity {
    private ProgressDialog mProgressDialog         = null;
    private List<String> mParserTotal              = null;
    private ViewPager mSummary                     = null;
    private CirclePageIndicator mIndicator         = null;
    private ListView        mListView              = null;
    private ImageView mLeftArrow                   = null;
    private ImageView mRight                       = null;
    private String pageData[]                      = null;//new String[4];
    private String pageData1[]                      = null;
    private String mMonthList[]                    = null;
    private DrawerAdapter mDrawerAdapter           = null;
    private File    mListOfFiles  []               = null;
    private String mCsvLocation                    = "";
    private LayoutInflater inflater                = null;
    private PieChart mChart                        = null;
    public static List<AWSServices> mListOfServices     = null;
    private ListView mDrawerList                   = null ;
    private DrawerLayout mDrawerLayout             = null;
    private ActionBarDrawerToggle mDrawerToggle    = null ;
    private String mActivityTitle                  = null;
    private DashBoardListAdapter mAdapterDashboard = null;
    private List<AwsLinkedAccounts>  mListOfLinkedAccounts    = null;
    private  String mAdminName                     = null;
    public static String mLinkedAccounName         = "";
    private TextView mDetailBill                   = null;
    private TextView mMonthDetail                  = null;
    private String mDetailOfAccount                = "Admin";
    public static String  mCurrentMonthFileName    = null;
    private ScrollView mScrollView                 = null;
    static  int mCounter                           = 0;
    private List<ParserModel> mParserModelList     = null;
    private Boolean mIsBillingAvalible             = false;
    public static HashMap<String,List<ParserModel>> mReportsOfAllMonths                        = null;
    public static HashMap<String,List<ParserModel>> mLinkedReportsOfAllMonths                  = null;
    private HashMap <String,String> mListOfSortForms                  = null;
    private AmazonS3Client mAmazonS3Client         = null;
    private BasicAWSCredentials mCredentials       = null;
    //private MonthYearPicker myp;
    private MonthYearPicker mMonthYearPicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        init();
        SetSortForms();
        NavigationDrawer();
        SetUpReportOfMonths();
        CsvParser csvParser = new CsvParser();
        for(File lFile : mListOfFiles){
                if(lFile.getAbsolutePath().contains("-aws-billing-csv-")){
                csvParser.execute(lFile.getAbsolutePath());
                mCurrentMonthFileName = lFile.getAbsolutePath();
                ShowProgressDialog(true);
                break;
                //ShowProgressDialog(true);
            }
        }
    }
    private void SetSortForms() {

        //self Added
        mListOfSortForms.put("AWS Data Transfer","Data Transfer");
        mListOfSortForms.put("Amazon Simple Queue Service","SQS");
        mListOfSortForms.put("Amazon Cognito","Cognito");
        mListOfSortForms.put("Amazon Mobile Analytics","Mobile Analytics");
        mListOfSortForms.put("Amazon Simple Notification Service","SNS");
        mListOfSortForms.put("Amazon Simple Storage Service","S3");
        mListOfSortForms.put("Amazon Elastic File System","EFS");
        mListOfSortForms.put("AWS Storage Gateway","Storage Gateway");
        mListOfSortForms.put("Amazon Glacier","Glacier");
        mListOfSortForms.put("Amazon CloudFront","CloudFront");
        mListOfSortForms.put("Amazon Elastic Block Store","EBS");
        mListOfSortForms.put("AWS Directory Service","Directory Service");
        mListOfSortForms.put("Amazon Identity and Access Management","IAM");
        mListOfSortForms.put("AWS CloudTrail","CloudTrail");
        mListOfSortForms.put("AWS Config","Config");
        mListOfSortForms.put("Amazon CloudWatch","CloudWatch");
        mListOfSortForms.put("AWS Key Management Service","KMS");
        mListOfSortForms.put("AWS CloudHSM","CloudHSM");
        mListOfSortForms.put("Amazon Elastic Compute Cloud","EC2");
        mListOfSortForms.put("Amazon EC2 Container Service","EC2 Container");
        mListOfSortForms.put("Elastic Load Balancing","Load Balancing");
        mListOfSortForms.put("AWS Lambda","Lambda");
        mListOfSortForms.put("Amazon Virtual Private Cloud","VPC");
        mListOfSortForms.put("Amazon Route 53","Route 53");
        mListOfSortForms.put("AWS Direct Connect","Direct Connect");
        mListOfSortForms.put("Amazon WorkSpaces","WorkSpaces");
        mListOfSortForms.put("Amazon WorkSpaces Application Manager","WAM");
        mListOfSortForms.put("Amazon WorkDocs","WorkDocs");
        mListOfSortForms.put("Amazon WorkMail (preview)","WorkMail");
        mListOfSortForms.put("Amazon Elastic MapReduce","EMR");
        mListOfSortForms.put("Amazon Kinesis","Kinesis");
        mListOfSortForms.put("AWS Data Pipeline","Data Pipeline");
        mListOfSortForms.put("Amazon Machine Learning","Machine Learning");

    }
    private void LoadAdminData(){
        mLinkedAccounName ="";
        SetSwipeInfo();
        GetBillOfMonth(mMonthList[mMonthList.length - 1]);
        mScrollView.fullScroll(ScrollView.FOCUS_UP);
        mScrollView.smoothScrollTo(0, 0);
    }
    private void LoadLinkedData(String lLinkedAccounName) {
        mLinkedAccounName   = lLinkedAccounName;
       /* ParserAsync lParserAsync = new ParserAsync();
        lParserAsync.execute();

        ShowProgressDialog(true);*/
        GetLinkedAccountData(lLinkedAccounName);
        UpdateUIForLinkedAccount();
       /* LinkedDAta lLinkedDAta = new LinkedDAta();
        lLinkedDAta.execute();*/
    }
    private void GetLinkedAccountData(String lLinkedAccounName) {
        mLinkedReportsOfAllMonths = new HashMap<String, List<ParserModel>>();
        Iterator it = mReportsOfAllMonths.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            List<ParserModel> lListParserModels = (List<ParserModel>) pair.getValue();
            List<ParserModel> llListParserModels = new ArrayList<ParserModel>();
            Iterator<ParserModel> presentsevicesIterator = lListParserModels.iterator();
            if (presentsevicesIterator != null) {
                while (presentsevicesIterator.hasNext()) {
                    ParserModel lParserModel = presentsevicesIterator.next();
                    if (((lParserModel.getmLinkedAccountName().trim()).equals(mLinkedAccounName.trim()))) {
                        llListParserModels.add(lParserModel);
                        Log.i(mLinkedAccounName.trim(), mLinkedAccounName.trim().length() + "");
                    }
                }
                if(llListParserModels.size()>0)
                mLinkedReportsOfAllMonths.put(pair.getKey().toString(), llListParserModels);
            }
        }
      /*  Iterator it1 = mLinkedReportsOfAllMonths.entrySet().iterator();
        while (it1.hasNext()) {
            Map.Entry pair = (Map.Entry) it1.next();
            //Log.d(pair.getKey().toString(), ((List<ParserModel>) pair.getValue()).size() + "");
        }*/
    }
    private void SetUpReportOfMonths() {
        mReportsOfAllMonths  = new HashMap<String ,List<ParserModel>>();
        mCsvLocation         = getFilesDir()+"/";
        File lFile           = new File(mCsvLocation);
        File[] lListOfFiles  = getFilesDir().listFiles();

        if(lListOfFiles.length>6){
            mListOfFiles = new File[6];
        }else{
            mListOfFiles = new File[lListOfFiles.length];
        }

        for (int index = 0;index < 6;index++){
            if(index < lFile.length()){
                mListOfFiles[index] = lListOfFiles[index];
            }
        }
        //mListOfFiles         = lFile.listFiles();
       /* List<File>  llListOFFiles       = new ArrayList<File >();
        for (int i =0;i<lListofFiles.length;i++){
         if(lListofFiles[i].length() > 50L){
             llListOFFiles.add(lListofFiles[i]);
             Log.i(lListofFiles[i].getName(), lListofFiles[i].length() + "");
         }
        }
        Log.i("llListOFFiles.size()",llListOFFiles.size()+"");
        mListOfFiles = new File[llListOFFiles.size()];
        for (int k = 0;k<llListOFFiles.size();k++){
            mListOfFiles[k] = llListOFFiles.get(k);
        }*/
      // mListOfFiles  =(File[]) llListOFFiles.toArray();// lFile.listFiles();
        pageData = new String[mListOfFiles.length];
        pageData1 = new String[mListOfFiles.length];
    }
    static class Utility {
        public static void setListViewHeightBasedOnChildren(ListView listView) {
            ListAdapter listAdapter = listView.getAdapter();
            if (listAdapter == null) {
                // pre-condition
                return;
            }

            int totalHeight = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            for (int i = 0; i < listAdapter.getCount(); i++) {
                View listItem = listAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                totalHeight += listItem.getMeasuredHeight();
            }

            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
            listView.setLayoutParams(params);
            listView.requestLayout();
        }
    }
    private void swipe() {
        final MyPagesAdapter myPagesAdapter = new MyPagesAdapter();
        mSummary.setAdapter(myPagesAdapter);
        mIndicator.setViewPager(this.mSummary);
       //mIndicator.setVisibility(View.INVISIBLE);
        if(mMonthList.length == 1){
            GetBillOfMonth(mMonthList[mMonthList.length-1]);
            mCurrentMonthFileName = mMonthList[mMonthList.length-1];
            String lFileName = mMonthList[mMonthList.length-1];
            String lNameContent[] = lFileName.split("-");
            int lLenth = lNameContent.length;
            String lMonthText = lNameContent[lLenth - 1];
            String lYearText = lNameContent[lLenth - 2];
            lMonthText = lMonthText.replaceAll(".csv", "");
            lMonthText = getMonth(Integer.parseInt(lMonthText));
            Log.i("lMonthText ",""+lMonthText + "-" + lYearText + " " + "Bill Amount");
            mMonthDetail.setText(lMonthText + "-" + lYearText + " " + "Bill Amount");
        }
        if(pageData.length > 1){
            mLeftArrow.setColorFilter(Color.parseColor("#f15a29"), PorterDuff.Mode.SRC_ATOP);
        }
        mIndicator.setFillColor(Color.parseColor("#f15a29"));
        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int lMonth = position + 1;
                mCounter = position;
                if (position == 0) {
                    mLeftArrow.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                } else {
                    mLeftArrow.setColorFilter(Color.parseColor("#f15a29"), PorterDuff.Mode.SRC_ATOP);
                }
                if ((myPagesAdapter.getCount() - 1) == mCounter) {
                    mRight.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                } else {
                    mRight.setColorFilter(Color.parseColor("#f15a29"), PorterDuff.Mode.SRC_ATOP);
                }
                if ((mMonthList.length - lMonth) >= 0) {
                    GetBillOfMonth(mMonthList[position]);
                    mCurrentMonthFileName = mMonthList[position];
                    String lFileName = mMonthList[position];
                    String lNameContent[] = lFileName.split("-");
                    int lLenth = lNameContent.length;
                    String lMonthText = lNameContent[lLenth - 1];
                    String lYearText = lNameContent[lLenth - 2];
                    lMonthText = lMonthText.replaceAll(".csv", "");
                    lMonthText = getMonth(Integer.parseInt(lMonthText));
                    //Log.i("lMonthText ",""+lMonthText + "-" + lYearText + " " + "Bill Amount");
                    mMonthDetail.setText(lMonthText + "-" + lYearText + " " + "Bill Amount");
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {
                //Log.i("", "state = " + state);
            }
        });
        mLeftArrow.setColorFilter(Color.parseColor("#f15a29"), PorterDuff.Mode.SRC_ATOP);
        //mRight.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        mLeftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCounter--;
                if (mCounter >= 0) {
                    if (mCounter == 0) {
                        // Log.i("mCounter gray ", "" + mCounter);
                        mLeftArrow.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                        mIndicator.setCurrentItem(mCounter);
                    } else {
                        // Log.i("mCounter f15a29 ", "" + mCounter);
                        mLeftArrow.setColorFilter(Color.parseColor("#f15a29"), PorterDuff.Mode.SRC_ATOP);
                        mIndicator.setCurrentItem(mCounter);
                    }
                }
            }
        });
        mRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCounter <= myPagesAdapter.getCount()-1) {
                mCounter++;
                if (mCounter == myPagesAdapter.getCount()-1) {
                    mRight.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    mIndicator.setCurrentItem(mCounter);
                } else {
                    mRight.setColorFilter(Color.parseColor("#f15a29"), PorterDuff.Mode.SRC_ATOP);
                    mIndicator.setCurrentItem(mCounter);
                }
                }

            }
        });
    }
    private String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month-1];
    }
    private void NavigationDrawer() {
        setupDrawer();
        final ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.dashboard_custom_actionbar, null);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER);
        TextView textviewTitle = (TextView) viewActionBar.findViewById(R.id.actionbar_textview);
        textviewTitle.setText("Dashboard");
        abar.setCustomView(viewActionBar, params);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);
        abar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#f15a29")));

    }
    private void PieChart(){

        mChart.setUsePercentValues(true);
        mChart.setDescription("");
        mChart.setDragDecelerationFrictionCoef(0.95f);
        mChart.setDrawHoleEnabled(true);
        mChart.setTransparentCircleColor(Color.TRANSPARENT);
        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(100f);
        mChart.setDrawCenterText(true);
        mChart.setRotationAngle(0);
        mChart.setRotationEnabled(false);
        mChart.setCenterText("Billing \n Information");
        mChart.setCenterTextColor(Color.parseColor("#000000"));
        mChart.setCenterTextSize(13.00f);
        setData();
        mChart.getLegend().setEnabled(false);

    }
    private void init() {
        //Image  view
        mLeftArrow  = (ImageView) findViewById(R.id.left);
        mRight      = (ImageView) findViewById(R.id.right);

        mScrollView = (ScrollView) findViewById(R.id.scrolview);

       //Pie Chart
        mChart = (PieChart) findViewById(R.id.chart1);

        mListView = (ListView) findViewById(R.id.lists3);
        //Swipe
        mSummary     = (ViewPager) findViewById(R.id.summary);
        mIndicator   = (CirclePageIndicator)this.findViewById(R.id.indicator);
        inflater     = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mParserTotal = new ArrayList<String>();

        //Drawer
        mDrawerList = (ListView)findViewById(R.id.navList);
        //mDrawerList.setDividerHeight(0);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        //TextView
        mMonthDetail = (TextView) findViewById(R.id.month);
        mDetailBill = (TextView) findViewById(R.id.details);
        mDetailBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent lBillIntent = new Intent(DashBoard.this, BillDetail.class);
                startActivity(lBillIntent);
            }
        });
        mListOfSortForms = new HashMap<String ,String>();
    }
    private void setData() {
        Boolean lIsVauesAvail = false;
        int lPieCharLenth = 0;
        Collections.sort(mListOfServices, new Comparator<AWSServices>() {
            @Override
            public int compare(AWSServices c1, AWSServices c2) {
                return Double.compare((Double.parseDouble(c2.getmTotalCost())),(Double.parseDouble(c1.getmTotalCost())));
            }
        });
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        if(mListOfServices.size() > 5){
            lPieCharLenth = 5;
        }else{
            lPieCharLenth = mListOfServices.size();
        }
        for (int i = 0; i < lPieCharLenth; i++){

            if(Float.parseFloat(mListOfServices.get(i).getmTotalCost() )> 1.0f){
                yVals1.add(new Entry(Float.parseFloat(mListOfServices.get(i).getmTotalCost()), i));
                lIsVauesAvail = true;
            }
        }
        ArrayList<String> xVals = new ArrayList<String>();
        for (int i = 0; i < lPieCharLenth; i++){
            if(Float.parseFloat(mListOfServices.get(i).getmTotalCost() )> 1.0f) {
                String lServices = "";
                if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("Amazon Elastic Compute Cloud")) {
                    lServices = "EC2";
                } else if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("AWS Data Transfer")) {
                    lServices = "DT";
                } else if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("Amazon Simple Notification Service")) {
                    lServices = "SNS";
                } else if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("Amazon Simple Storage Service")) {
                    lServices = "S3";
                } else if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("AWS Key Management Service")) {
                    lServices = "MS";
                } else if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("Amazon Simple Queue Service")) {
                    lServices = "SQS";
                } else if (mListOfServices.get(i).getmNAme().equalsIgnoreCase("Amazon Cognito")) {
                    lServices = "CT";
                } else {
                    lServices = mListOfServices.get(i).getmNAme();
                }
                xVals.add(lServices);
            }
   }
       ArrayList<Integer> colors = new ArrayList<Integer>();
       for (int i = 0; i < lPieCharLenth; i++){
           if (Float.parseFloat(mListOfServices.get(i).getmTotalCost()) > 1.0f) {
               Random rnd = new Random();
               int color = Color.rgb(rnd.nextInt(200), rnd.nextInt(256), rnd.nextInt(100));
               colors.add(color);
           }
       }
        if((xVals.size()<1)||(yVals1.size()<1)){
            xVals.add("");
            yVals1.add(new Entry(1, 0));
            colors.add(Color.GRAY);
            //Log.i("","Billing Result is zero");
        }
        PieDataSet dataSet = new PieDataSet(yVals1, "Billing Result");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        if(lIsVauesAvail)
        dataSet.setValueTextSize(12f);
        else
        dataSet.setValueTextSize(0f);
        dataSet.setColors(colors);
        PieData data = new PieData(xVals, dataSet);
        if(lIsVauesAvail)
        data.setValueFormatter(new PercentFormatter());
        mChart.setData(data);
        mChart.invalidate();
    }
    private void ShowProgressDialog(boolean isshow) {
        if(isshow){
            mProgressDialog = new ProgressDialog(DashBoard.this);
            mProgressDialog.setMessage("Processing Bills Please Wait....");
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        }else{
            if(mProgressDialog!=null) {
                mProgressDialog.cancel();
                mProgressDialog = null;
            }
        }
    }
    private void SetBillingInfo(String lMonth) {
          ShowDataOnUI();
    }
    private void SetSwipeInfo() {
        String lPayerAccountId          = "";
        String lTotalCostForCMonth      = "";
        String lCurrency                = "";
        int j      = mListOfFiles.length-1;
        pageData   = new String[mListOfFiles.length];
        pageData1   = new String[mListOfFiles.length];
        mMonthList = new String[mListOfFiles.length];
        for(int i = mListOfFiles.length-1 ; i>=0 ; i--) {
             lTotalCostForCMonth      = GetTotalBillOfCMonth(mListOfFiles[i].getAbsolutePath());
             lCurrency                = GetCurrency(mListOfFiles[i].getAbsolutePath());
             lPayerAccountId          = GetPayerAccountID(mListOfFiles[i].getAbsolutePath());
             Double d = 0.0;
             try {
                 d = Double.parseDouble(lTotalCostForCMonth);
             }catch (NumberFormatException e){

             }
             String  ltotal = String.format("%.2f", d);
             pageData[j]    = "Payer Account Name \n" + lPayerAccountId;//
             pageData1[j]   = ltotal + lCurrency;
             mMonthList[j]  = mListOfFiles[i].getAbsolutePath();
            j--;
        }
        swipe();

        mSummary.setCurrentItem(pageData.length - 1);
    }
    private String GetCurrency(String lMonth) {
        String lCurrency = "";
        if(mLinkedAccounName.equals("")) {
            List<ParserModel> lParserModelList = mReportsOfAllMonths.get(lMonth);
            if (lParserModelList != null) {
                for (ParserModel parserModel : lParserModelList) {
                    if ((parserModel.getmCurrencyCode() != null) && (!parserModel.getmCurrencyCode().isEmpty()) && (parserModel.getmCurrencyCode().length() >= 2)) {
                        lCurrency = parserModel.getmCurrencyCode();
                        break;
                    }
                }

            }
        }else{

        }
        return lCurrency;
    }
    private String GetTotalBillOfCMonth(String lMonth) {
        String lTotalCostForCMonth = "";
        Double lTotalcost = 0.0;
        if(mLinkedAccounName.equals("")) {
            List<ParserModel> lParserModelList = mReportsOfAllMonths.get(lMonth);
            if (lParserModelList != null) {
                for (ParserModel parserModel : lParserModelList) {
                    if (parserModel.getmLinkedAccountName().trim().equals(mLinkedAccounName.trim())) {
                        if ((parserModel.getmTotalCost() != null) && (!parserModel.getmTotalCost().isEmpty()) && (parserModel.getmTotalCost().length() >= 1)) {
                            try {
                                lTotalcost += Double.parseDouble(parserModel.getmTotalCost());
                                //Log.i("GetTotalBillOfCMonth", mLinkedAccounName.trim());
                            } catch (NumberFormatException e) {

                            }
                        }
                    }
                }
            }
        }else {
            List<ParserModel> lParserModelList = mLinkedReportsOfAllMonths.get(lMonth);
            if (lParserModelList != null) {
                for (ParserModel parserModel : lParserModelList) {
                    if (parserModel.getmLinkedAccountName().trim().equals(mLinkedAccounName.trim())) {
                        if ((parserModel.getmTotalCost() != null) && (!parserModel.getmTotalCost().isEmpty()) && (parserModel.getmTotalCost().length() >= 1)) {
                            try {
                                lTotalcost += Double.parseDouble(parserModel.getmTotalCost());
                                //Log.i("GetTotalBillOfCMonth", mLinkedAccounName.trim());
                            } catch (NumberFormatException e) {

                            }
                        }
                    }
                    /*Iterator it1 = mLinkedReportsOfAllMonths.entrySet().iterator();
                    while (it1.hasNext()) {
                        Map.Entry pair = (Map.Entry) it1.next();
                        //Log.d(pair.getKey().toString(), ((List<ParserModel>) pair.getValue()).size() + "");
                    }*/
                }
            }
        }
        lTotalCostForCMonth = lTotalcost.toString();
        return lTotalCostForCMonth;
    }
    private String GetPayerAccountID(String lMonth) {
        String lPayerAccountId = "";
        if(mLinkedAccounName.equals("")){
        List <ParserModel>lParserModelList  = mReportsOfAllMonths.get(lMonth);
        if(lParserModelList != null) {
            Collections.sort(lParserModelList, new BillingListSortBasedOnPayerAccountName());
            for (ParserModel parserModel : lParserModelList) {
                if ((parserModel.getmPayerAccountName() != null) && (!parserModel.getmPayerAccountName().isEmpty()) && (parserModel.getmPayerAccountName().length() >= 4)) {
                    lPayerAccountId = parserModel.getmPayerAccountName();
                    break;
                }
            }
        }
        }else {
            List <ParserModel>lParserModelList  = mLinkedReportsOfAllMonths.get(lMonth);
            if(lParserModelList != null) {
                Collections.sort(lParserModelList, new BillingListSortBasedOnPayerAccountName());
                for (ParserModel parserModel : lParserModelList) {
                    if ((parserModel.getmPayerAccountName() != null) && (!parserModel.getmPayerAccountName().isEmpty()) && (parserModel.getmPayerAccountName().length() >= 4)) {
                        lPayerAccountId = parserModel.getmPayerAccountName();
                        break;
                    }
                }
            }
        }
        return lPayerAccountId;
    }
    private void ShowDataOnUI() {
       PieChart();
    }
    private void addDrawerItems() {

        if(mAdminName == null){
            mAdminName = GetAdminName();
        }
        List<String> mListOfDrawer     = new ArrayList<String >();
        mListOfDrawer.add(0,"View\n" +
                "Billing\n" +
                "\n" +
                "My Account");
        mListOfDrawer.add(1,"Admin");
        mListOfDrawer.add(2,   this.mAdminName);
        mListOfDrawer.add(3, "LinkedAccounts");
        for(int i = 0 ,j=4;i < mListOfLinkedAccounts.size();i++){
            mListOfDrawer.add(j, mListOfLinkedAccounts.get(i).getmAccountOwnerName());
            j++;
        }
        mDrawerAdapter   = new DrawerAdapter(this,mAdminName,mListOfDrawer);
        mDrawerList.setAdapter(mDrawerAdapter);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView lAccountName = (TextView) view.findViewById(R.id.textView2);
                if ((lAccountName != null) && (position < 3) && (position > 0)) {
                    mDetailOfAccount = "Admin";
                    LoadAdminData();
                } else if ((position > 3) && (lAccountName != null)) {
                    mDetailOfAccount = lAccountName.getText().toString();
                    //Log.i("lAccountName.getText().toString()",lAccountName.getText().toString());
                    LoadLinkedData(lAccountName.getText().toString());
                }
                mDrawerLayout.closeDrawers();
            }
        });
    }
    private String GetAdminName() {
        String lAdminName = "";
        Iterator it = mReportsOfAllMonths.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            List<ParserModel> lParserModelList = (List<ParserModel>) pair.getValue();
            Collections.sort(lParserModelList, new BillingListSortBasedOnPayerAccountName());
            for (ParserModel parserModel : lParserModelList) {
                if ((parserModel.getmPayerAccountName() != null) && (!parserModel.getmPayerAccountName().isEmpty()) && (parserModel.getmPayerAccountName().length() >= 1)) {
                    lAdminName = parserModel.getmPayerAccountName();
                    break;
                }
            }
            if(lAdminName!= null){
                break;
            }
        }
        return lAdminName;

    }
    private void UpdateUIForLinkedAccount() {
        SetSwipeInfoForLinkedAccont();
        ExtractPieChartInfo(mMonthList[mMonthList.length - 1]);
        SetBillingInfo(mMonthList[mMonthList.length - 1]);
        ShowListOfServices();
    }
    private void SetSwipeInfoForLinkedAccont() {
        String lPayerAccountId          = "";
        String lTotalCostForCMonth      = "";
        String lCurrency                = "";
        int j = 0;
        String lpagadata []   = new String[ mListOfFiles.length];
        String llpagadata []  = new String[ mListOfFiles.length];
        String lMonthList []  = new String[ mListOfFiles.length];
        for(int i = mListOfFiles.length-1 ; i>=0 ; i--) {
            lTotalCostForCMonth      = GetTotalBillOfCMonth(mListOfFiles[i].getAbsolutePath());
            lCurrency                = GetCurrency(mListOfFiles[i].getAbsolutePath());
            lPayerAccountId          = LinkenkedAccontName(mListOfFiles[i].getAbsolutePath());
            Double d                 = Double.parseDouble(lTotalCostForCMonth);
            String  ltotal           = String.format("%.2f",d);
            if((lPayerAccountId.length()>0)&&(!lPayerAccountId.isEmpty())){
                lpagadata[j]  = "Payer Account Name \n" + lPayerAccountId; //
                llpagadata[j] = ltotal + lCurrency;
                lMonthList[j] = mListOfFiles[i].getAbsolutePath();
                 j++;
            }
        }
        pageData    = new String[j];
        pageData1   = new String[j];
        mMonthList  = new String[j];
        //Log.i("pagdata j",j+"");
        for (int k=0; k < j ;k++){
                pageData[k]          = lpagadata[k];
                pageData1[k]         = llpagadata[k];
                mMonthList[k]        = lMonthList[k];
        }
        List<String> list = Arrays.asList(pageData);
        Collections.reverse(list);
        List<String> list0 = Arrays.asList(pageData1);
        Collections.reverse(list0);
        pageData = (String[]) list.toArray();
        pageData1 = (String[]) list0.toArray();
        List<String> list1 = Arrays.asList(mMonthList);
        Collections.reverse(list1);
        mMonthList = (String[]) list1.toArray();
        swipe();
        mSummary.setCurrentItem(pageData.length - 1);
    }
    private String LinkenkedAccontName(String absolutePath) {
        String lPayerAccountId = "";
        if(mLinkedAccounName.equals("")) {
            List<ParserModel> lParserModelList = mReportsOfAllMonths.get(absolutePath);
            if (lParserModelList != null) {
                Collections.sort(lParserModelList, new BillingListSortBasedOnLinkedAccountName());
                for (ParserModel parserModel : lParserModelList) {
                    if ((parserModel.getmLinkedAccountName() != null) && (!parserModel.getmLinkedAccountName().isEmpty()) && (parserModel.getmLinkedAccountName().length() >= 0)) {
                        lPayerAccountId = parserModel.getmLinkedAccountName();
                        break;
                    }
                }
            }
        }else{
            List<ParserModel> lParserModelList = mLinkedReportsOfAllMonths.get(absolutePath);
            if (lParserModelList != null) {
                Collections.sort(lParserModelList, new BillingListSortBasedOnLinkedAccountName());
                for (ParserModel parserModel : lParserModelList) {
                    if ((parserModel.getmLinkedAccountName() != null) && (!parserModel.getmLinkedAccountName().isEmpty()) && (parserModel.getmLinkedAccountName().length() >= 0)) {
                        lPayerAccountId = parserModel.getmLinkedAccountName();
                        break;
                    }
                }
            }
        }
        return lPayerAccountId;

    }
    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle("View Billing");
                invalidateOptionsMenu();
            }
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mActivityTitle);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }
    private void ExtractPieChartInfo(String lMonth) {
        if(mLinkedAccounName.equals("")) {
            List<ParserModel> lParserModelList = mReportsOfAllMonths.get(lMonth);
            mListOfServices = new ArrayList<AWSServices>();
            if (lParserModelList != null) {
                Collections.sort(lParserModelList, new BillingListSortBasedOnProductName());
                for (ParserModel parserModel : lParserModelList) {
                    if ((parserModel.getmProductName() != null) && (!parserModel.getmProductName().isEmpty()) && (parserModel.getmProductName().length() >= 1)) {
                        if (parserModel.getmLinkedAccountName().trim().equals(mLinkedAccounName.trim())) {
                            if (mListOfServices.size() == 0) {
                                AWSServices lAwsServices = new AWSServices();
                                lAwsServices.setmNAme(parserModel.getmProductName());
                                lAwsServices.setmTotalCost(parserModel.getmTotalCost());
                                // Log.i("lAwsServices first", "NAME = " + lAwsServices.getmNAme() + " mTotalCOst = " + lAwsServices.getmTotalCost());
                                mListOfServices.add(lAwsServices);
                            } else {
                                Boolean lIspresent = false;
                                Iterator<AWSServices> presentsevicesIterator = mListOfServices.iterator();
                                if (presentsevicesIterator != null) {
                                    while (presentsevicesIterator.hasNext()) {
                                        AWSServices awsServices = presentsevicesIterator.next();
                                        if ((awsServices.getmNAme()).trim().equals(parserModel.getmProductName().trim())) {
                                            presentsevicesIterator.remove();
                                            Double lTotal = 0.0;
                                            lTotal += Double.parseDouble(awsServices.getmTotalCost())
                                                    + Double.parseDouble(parserModel.getmTotalCost());
                                            awsServices.setmTotalCost(lTotal.toString());
                                            mListOfServices.add(awsServices);
                                            lIspresent = true;
                                        }
                                    }
                                }
                                if (!lIspresent) {
                                    AWSServices lAwsServices = new AWSServices();
                                    lAwsServices.setmNAme(parserModel.getmProductName());
                                    lAwsServices.setmTotalCost(parserModel.getmTotalCost());
                                    mListOfServices.add(lAwsServices);
                                }

                            }
                        }
                    }
                }
            }
        }else{
            List<ParserModel> lParserModelList = mLinkedReportsOfAllMonths.get(lMonth);
            mListOfServices = new ArrayList<AWSServices>();
            if (lParserModelList != null) {
                Collections.sort(lParserModelList, new BillingListSortBasedOnProductName());
                for (ParserModel parserModel : lParserModelList) {
                    if ((parserModel.getmProductName() != null) && (!parserModel.getmProductName().isEmpty()) && (parserModel.getmProductName().length() >= 1)) {
                        if (parserModel.getmLinkedAccountName().trim().equals(mLinkedAccounName.trim())) {
                            if (mListOfServices.size() == 0) {
                                AWSServices lAwsServices = new AWSServices();
                                lAwsServices.setmNAme(parserModel.getmProductName());
                                lAwsServices.setmTotalCost(parserModel.getmTotalCost());
                                // Log.i("lAwsServices first", "NAME = " + lAwsServices.getmNAme() + " mTotalCOst = " + lAwsServices.getmTotalCost());
                                mListOfServices.add(lAwsServices);
                            } else {
                                Boolean lIspresent = false;
                                Iterator<AWSServices> presentsevicesIterator = mListOfServices.iterator();
                                if (presentsevicesIterator != null) {
                                    while (presentsevicesIterator.hasNext()) {
                                        AWSServices awsServices = presentsevicesIterator.next();
                                        if ((awsServices.getmNAme()).trim().equals(parserModel.getmProductName().trim())) {
                                            presentsevicesIterator.remove();
                                            Double lTotal = 0.0;
                                            lTotal += Double.parseDouble(awsServices.getmTotalCost())
                                                    + Double.parseDouble(parserModel.getmTotalCost());
                                            awsServices.setmTotalCost(lTotal.toString());
                                            mListOfServices.add(awsServices);
                                            lIspresent = true;
                                        }
                                    }
                                }
                                if (!lIspresent) {
                                    AWSServices lAwsServices = new AWSServices();
                                    lAwsServices.setmNAme(parserModel.getmProductName());
                                    lAwsServices.setmTotalCost(parserModel.getmTotalCost());
                                    mListOfServices.add(lAwsServices);
                                }

                            }
                        }
                    }
                }
            }
        }
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dash_board, menu);
        return true;
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    @Override
    public boolean  onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_change_bucket_usage) {
            if(isNetworkAvaliable(DashBoard.this)) {
                for(File file: getFilesDir().listFiles())
                    file.delete();
                Intent intent = new Intent(DashBoard.this,AutoConfiguration.class);
                startActivity(intent);
                finish();
                return true;
            }else{
                final Dialog dialog = new Dialog(DashBoard.this);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("");
                TextView text = (TextView) dialog.findViewById(R.id.text);
                text.setText("The Internet connection appears to be offline Please Check Your Internet Settings");
                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
        }
        if (id == R.id.action_logout_usage) {
            for(File file: getFilesDir().listFiles())
                file.delete();
            SharedPreferences Loginstate    = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
            SharedPreferences.Editor value  = Loginstate.edit();
            value.putBoolean("LoginState", false);
            value.commit();
            Intent intent = new Intent(DashBoard.this,LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        if(id == R.id.action_calender) {
            if(isNetworkAvaliable(DashBoard.this)) {
                Calendar cal = Calendar.getInstance();
                int lYear = cal.get(Calendar.YEAR);
                MonthYearPicker.MAX_YEAR = lYear;
                mMonthYearPicker = new MonthYearPicker(DashBoard.this);
                mMonthYearPicker.build(new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DownLoadFilesFromDate(mMonthYearPicker.getSelectedMonth(), mMonthYearPicker.getSelectedYear());
                    }
                }, null);
                mMonthYearPicker.show();
            }else{
                final Dialog dialog = new Dialog(DashBoard.this);
                dialog.setContentView(R.layout.custom);
                dialog.setTitle("");
                TextView text = (TextView) dialog.findViewById(R.id.text);
                text.setText("The Internet connection appears to be offline Please Check Your Internet Settings");
                Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
                dialogButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                    }
                });
                dialog.show();
            }
            return true;
        }
        // Activate the navigation drawer toggle
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void DownLoadFilesFromDate(int selectedMonth, int selectedYear) {
        Calendar calendar = Calendar.getInstance();
        int lmonth = calendar.get(Calendar.MONTH);
        int lyear  = calendar.get(Calendar.YEAR);;
        if((selectedMonth > lmonth)&&(lyear== selectedYear)){
          Toast.makeText(DashBoard.this,"Please Select Valid Month",Toast.LENGTH_LONG).show();
      }else {
            SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate", MODE_MULTI_PROCESS);
            String lAccess_key = lSharedPreferences.getString("accesskey", "error");
            String lSecret_key = lSharedPreferences.getString("secretkey", "error");
            mCredentials         = new BasicAWSCredentials(lAccess_key, lSecret_key);
            mAmazonS3Client      = new AmazonS3Client(mCredentials);
            DowloadFromToto lDowloadFromToto = new DowloadFromToto();
            lDowloadFromToto.execute(selectedMonth + 1, selectedYear);
            ShowProgressDialog(true);
        }
    }
    public void show(View view) {
        mMonthYearPicker.show();
    }
    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }
    private void    ShowListOfServices(){
        //Log.i("mListOfServices -",mListOfServices.size()+"")
        Collections.sort(mListOfServices, new Comparator<AWSServices>() {
            @Override
            public int compare(AWSServices c1, AWSServices c2) {
                return Double.compare((Double.parseDouble(c2.getmTotalCost())),(Double.parseDouble(c1.getmTotalCost())));
            }
        });
        mAdapterDashboard  = new DashBoardListAdapter(DashBoard.this,mListOfServices);
        mListView.setAdapter(mAdapterDashboard);
        Utility.setListViewHeightBasedOnChildren(mListView);
        //mListView.setFocusable(false);
    }
    @Override
    public  void onBackPressed() {
        Log.i("","onBackPressed");
        super.onBackPressed();
    }
    private void GetBillOfMonth(String absolutePath) {
        ExtractPieChartInfo(absolutePath);
        SetBillingInfo(absolutePath);
        ShowListOfServices();
    }
    class MyPagesAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return pageData.length;
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            //
             View page = inflater.inflate(R.layout.textview_swipe, null);
             TextView tv = ((TextView) page.findViewById(R.id.textMessage));
             tv.setText(pageData[position]);
             tv.setGravity(Gravity.CENTER);
             ((ViewPager) container).addView(page, 0);
            //
            View page1 = inflater.inflate(R.layout.textview_swipe, null);
            TextView tv1 = ((TextView) page1.findViewById(R.id.textMessage1));
            tv1.setGravity(Gravity.CENTER);
            ((ViewPager) container).addView(page1, 1);

            View page2 = inflater.inflate(R.layout.textview_swipe, null);
            TextView tv2 = ((TextView) page.findViewById(R.id.textMessage2));
            tv2.setText(pageData1[position]);
            tv.setGravity(Gravity.CENTER);
            ((ViewPager) container).addView(page2, 2);

            return page;
        }
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0==(View)arg1;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
             object=null;
        }
    }
    class DowloadFromToto extends AsyncTask<Integer ,Void,Void>{
        @Override
        protected Void doInBackground(Integer... params) {
            int lmonth = params[0];
            int lyear = params[1];
            mIsBillingAvalible = false;
            List <String>lListOfSelectMonths = new ArrayList<String>();
            String lDate = "";
            if (lmonth > 9) {
                lDate = lyear + "-" + lmonth;
            } else {
                lDate = lyear + "-0" + lmonth;
            }
            int lIndexOfCurrentMonth = 0;
            SharedPreferences BucketConfig = getSharedPreferences("BucketConfig", MODE_MULTI_PROCESS);
            SharedPreferences.Editor value = BucketConfig.edit();
            String lBucketName = BucketConfig.getString("bucketname", "");
            ObjectListing lList = null;
            try {
                lList = mAmazonS3Client.listObjects(lBucketName, "");
            } catch (AmazonS3Exception e) {

            }
            String lFileContains = "-aws-billing-csv-" + lDate;
            Log.i("lFileContains",lFileContains);
            if (lList != null) {
                List<S3ObjectSummary> lListObjectSummaries = lList.getObjectSummaries();
                for (S3ObjectSummary s3ObjectSummary : lListObjectSummaries) {
                    lIndexOfCurrentMonth++;
                    if((s3ObjectSummary.getKey().toString().contains(lFileContains))) {
                        mIsBillingAvalible = true;
                        break;
                    }
                }
            }
            if (mIsBillingAvalible) {
                List<String> lListOFCsvs = new ArrayList<String>();
                List<S3ObjectSummary> llListObjectSummaries = lList.getObjectSummaries();
                for (S3ObjectSummary s3ObjectSummary : llListObjectSummaries) {
                    if ((s3ObjectSummary.getKey().toString().contains("-aws-billing-csv-"))) {
                        lListOFCsvs.add(s3ObjectSummary.getKey().toString());
                        Log.i("Name ", s3ObjectSummary.getKey().toString());
                    }
                }
                int lIndexOfCurrentMonthstart = 0;
                if(lIndexOfCurrentMonth > 6){
                    lIndexOfCurrentMonthstart = lIndexOfCurrentMonth-6;
                }
                for (int filenameindex = lIndexOfCurrentMonthstart ; filenameindex < lIndexOfCurrentMonth; filenameindex++) {

                    if(filenameindex < lListOFCsvs.size()) {
                        if ((lListOFCsvs.get(filenameindex).contains("-aws-billing-csv-"))) {

                            lListOfSelectMonths.add(lListOFCsvs.get(filenameindex));
                            FileOutputStream outputStream;
                            File file = new File(getFilesDir().getAbsolutePath() + "/" + lListOFCsvs.get(filenameindex));
                            try {
                                if (!file.exists()) {
                                    file.createNewFile();
                                    file.setWritable(true);
                                outputStream = new FileOutputStream(file);
                                GetObjectRequest getObjectRequest = new GetObjectRequest(lBucketName, lListOFCsvs.get(filenameindex));
                                S3Object object = mAmazonS3Client.getObject((getObjectRequest));
                                InputStream objectData = object.getObjectContent();
                                byte[] buffer = new byte[1024];
                                int read = 0;
                                //objectData.
                                // IOUtils.copy(objectData, outputStream);
                                while ((read = objectData.read(buffer)) != -1) {
                                    outputStream.write(buffer, 0, read);
                                }
                                objectData.close();
                                outputStream.close();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {

                            }
                        }
                    }
                }
                HashMap<String,File> lMapOFFiles = new HashMap<String,File>();
                File[] lListOfFiles = getFilesDir().listFiles();
                for (File lFile :lListOfFiles){
                    lMapOFFiles.put(lFile.getName(),lFile);
                }
                mListOfFiles = new File[lListOfSelectMonths.size()];
                for (int lindex=0;lindex<lListOfSelectMonths.size();lindex++){

                    mListOfFiles[lindex] = (File)lMapOFFiles.get(lListOfSelectMonths.get(lindex));

                }
                pageData  = new String[mListOfFiles.length];
                pageData1 = new String[mListOfFiles.length];
                Log.i("mListOfFiles  ",mListOfFiles.length+"");
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(mIsBillingAvalible){
            CsvParser csvParser = new CsvParser();
            for(File lFile : mListOfFiles){
                if(lFile.getAbsolutePath().contains("-aws-billing-csv-")){
                    csvParser.execute(lFile.getAbsolutePath());
                    mCurrentMonthFileName = lFile.getAbsolutePath();
                    break;
                  }
               }
            }else
            {
                ShowProgressDialog(false);
                Toast.makeText(DashBoard.this,"Sorry There is No Billing Infromation For Selected Month!",Toast.LENGTH_LONG).show();
            }
            super.onPostExecute(aVoid);
        }
    }
    class CsvParser extends AsyncTask<String,Void,Void>{
        @Override
        protected Void doInBackground(String... params) {
            //Log.d("mListOfFiles.length",""+mListOfFiles.length);
            mListOfLinkedAccounts                                                  = new ArrayList<AwsLinkedAccounts>();
            HashMap<String, AwsLinkedAccounts> lLisStringAwsLinkedAccountsHashMap  = new HashMap<String, AwsLinkedAccounts>();
            mAdminName = null;
            for(int j =0 ;j<mListOfFiles.length;j++){
                Log.i("mListOfFiles "+j ,mListOfFiles[j].getName());
                mParserModelList = new ArrayList<ParserModel>();
                BufferedReader lBufferedReader = null;
                String lLine = "";
                String lCvsSplitBy = "\",";
                List <String []>myEntries =null;
                try {
                   // Log.i("FileName", mListOfFiles[j].getAbsolutePath() + "  " + mListOfFiles[j].getTotalSpace());
                    mListOfFiles[j].setExecutable(true);
                    mListOfFiles[j].setWritable(true);
                    CSVReader csvReader = new CSVReader(new FileReader(mListOfFiles[j].getAbsolutePath()), ',', '"', 1);
                    myEntries = csvReader.readAll();
                    Double lTotalcost = 0.0;
                     for (int i = 0; i < myEntries.size()-6; i++) {
                        String lToatalRows[] = (String[]) myEntries.get(i);
                        if (lToatalRows.length == 29) {
                            ParserModel lParserModel = new ParserModel();
                            String productCode =  mListOfSortForms.get(lToatalRows[13].replaceAll("\"", ""));
                            if(productCode!=null){
                                lParserModel.setmProductName(productCode);
                            }else {
                                lParserModel.setmProductName(lToatalRows[13].replaceAll("\"", ""));
                            }
                            lParserModel.setmUsageType(lToatalRows[15].replaceAll("\"", ""));
                            lParserModel.setmItemDescription(lToatalRows[18].replaceAll("\"", ""));
                            lParserModel.setmUsageQuantity(lToatalRows[21].replaceAll("\"", ""));
                            lParserModel.setmTotalCost(lToatalRows[28].replaceAll("\"", ""));
//                            lTotalcost += Double.parseDouble(lParserModel.getmTotalCost());
                            lParserModel.setmPayerAccountId(lToatalRows[1].replaceAll("\"", ""));
                            lParserModel.setmBillingPeriodStartDate(lToatalRows[5].replaceAll("\"", ""));
                            lParserModel.setmBillingPeriodEndDate(lToatalRows[6].replaceAll("\"", ""));
                            lParserModel.setmInvoiceDate(lToatalRows[7].replaceAll("\"", ""));
                            if(mAdminName == null){
                                mAdminName         = lToatalRows[8].replaceAll("\"", "");
                            }
                            //Log.i("Payer Account NAme ",lToatalRows[8].replaceAll("\"", ""));
                            lParserModel.setmPayerAccountName(lToatalRows[8].replaceAll("\"", ""));
                            lParserModel.setmLinkedAccountName(lToatalRows[9].replaceAll("\"", ""));
                            lParserModel.setmTaxationAddress(lToatalRows[10].replaceAll("\"", ""));
                            lParserModel.setmSellerOfRecord(lToatalRows[14].replaceAll("\"", ""));
                            lParserModel.setmCurrencyCode(lToatalRows[23].replaceAll("\"", ""));
                            mParserModelList.add(lParserModel);
                            String lLinkedAccountName = lParserModel.getmLinkedAccountName();
                            if ((lParserModel.getmLinkedAccountName() != null) && (!lParserModel.getmLinkedAccountName().isEmpty()) && (lParserModel.getmLinkedAccountName().length() > 0)) {

                               // Log.i("",lParserModel.getmLinkedAccountName());
                                AwsLinkedAccounts lAwsLinkedAccounts = lLisStringAwsLinkedAccountsHashMap.get(lLinkedAccountName);
                                if (lAwsLinkedAccounts == null) {
                                    lAwsLinkedAccounts = new AwsLinkedAccounts();
                                    lAwsLinkedAccounts.setmAccountOwnerName(lParserModel.getmLinkedAccountName());
                                    lAwsLinkedAccounts.setmTotalCost(lParserModel.getmTotalCost());
                                    mListOfLinkedAccounts.add(lAwsLinkedAccounts);
                                    lLisStringAwsLinkedAccountsHashMap.put(lLinkedAccountName, lAwsLinkedAccounts);
                                } else {
                                    Double lTotal = Double.parseDouble(lAwsLinkedAccounts.getmTotalCost());
                                    lTotal += Double.parseDouble(lParserModel.getmTotalCost());
                                    String ltotal = String.format("%.2f", lTotal);
                                    lAwsLinkedAccounts.setmTotalCost(ltotal);
                                }
                                //Log.i("mListOfLinkedAccounts", "Size = " + mListOfLinkedAccounts.size());
                            }
                        } else {
                            mParserTotal.add(lLine);
                            //
                        }
                    }
                    //Log.i("TotalCost ", mListOfFiles[j].getName().toString() + lTotalcost.toString());
                    if(mParserModelList.size()>0)
                    mReportsOfAllMonths.put(mListOfFiles[j].getAbsolutePath(), mParserModelList);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }  finally {
                    if (lBufferedReader != null) {
                        try {
                            lBufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
                SetSwipeInfo();
                GetBillOfMonth(mMonthList[mMonthList.length - 1]);
                addDrawerItems();
                ShowProgressDialog(false);
                mScrollView.fullScroll(ScrollView.FOCUS_UP);
                mScrollView.smoothScrollTo(0, 0);
            }

    }
    class ParserAsync extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            mListOfLinkedAccounts                                                  = new ArrayList<AwsLinkedAccounts>();
            HashMap<String, AwsLinkedAccounts> lLisStringAwsLinkedAccountsHashMap  = new HashMap<String, AwsLinkedAccounts>();
            for(int j =0 ;j<mListOfFiles.length;j++) {
                mParserModelList = new ArrayList<ParserModel>();
                BufferedReader lBufferedReader = null;
                String lLine = "";
                String lCvsSplitBy = "\",";
                //13,15,18,21,28
                try {
                    CSVReader csvReader = new CSVReader(new FileReader(mListOfFiles[j].getAbsolutePath()), ',', '"', 1);
                    // Log.i("", params[0]);
                    List myEntries = csvReader.readAll();
                    //   Log.i("myEntries","Size "+myEntries.size());
                    for (int i = 0; i < myEntries.size()-6; i++) {
                        String lToatalRows[] = (String[]) myEntries.get(i);
                        if (lToatalRows.length == 29) {
                            // Log.i("myEntries", "Size  of lines " + lToatalRows.length);
                            ParserModel lParserModel = new ParserModel();
/*
                            String productCode =  mListOfSortForms.get(lToatalRows[13].replaceAll("\"", ""));
                            if(productCode!=null){
                                lParserModel.setmProductName(productCode);
                            }else {
                                lParserModel.setmProductName(lToatalRows[13].replaceAll("\"", ""));
                            }

                            lParserModel.setmUsageType(lToatalRows[15].replaceAll("\"", ""));
                            lParserModel.setmItemDescription(lToatalRows[18].replaceAll("\"", ""));
                            lParserModel.setmUsageQuantity(lToatalRows[21].replaceAll("\"", ""));
                            // Log.i("Totalcost = ",lToatalRows[28]);
                            lParserModel.setmTotalCost(lToatalRows[28].replaceAll("\"", ""));
                            lParserModel.setmPayerAccountId(lToatalRows[1].replaceAll("\"", ""));
                            lParserModel.setmBillingPeriodStartDate(lToatalRows[5].replaceAll("\"", ""));
                            lParserModel.setmBillingPeriodEndDate(lToatalRows[6].replaceAll("\"", ""));
                            lParserModel.setmInvoiceDate(lToatalRows[7].replaceAll("\"", ""));
                            lParserModel.setmPayerAccountName(lToatalRows[8].replaceAll("\"", ""));
                            lParserModel.setmLinkedAccountName(lToatalRows[9].replaceAll("\"", ""));
                            lParserModel.setmTaxationAddress(lToatalRows[10].replaceAll("\"", ""));
                            lParserModel.setmSellerOfRecord(lToatalRows[14].replaceAll("\"", ""));
                            lParserModel.setmCurrencyCode(lToatalRows[23].replaceAll("\"", ""));
                            mParserModelList.add(lParserModel);

                            String lLinkedAccountName = lParserModel.getmLinkedAccountName();
                            if ((lParserModel.getmLinkedAccountName() != null) && (!lParserModel.getmLinkedAccountName().isEmpty()) && (lParserModel.getmLinkedAccountName().length() > 0))
                            {

                                // Log.i("",lParserModel.getmLinkedAccountName());
                                AwsLinkedAccounts lAwsLinkedAccounts = lLisStringAwsLinkedAccountsHashMap.get(lLinkedAccountName);
                                if (lAwsLinkedAccounts == null) {
                                    lAwsLinkedAccounts = new AwsLinkedAccounts();
                                    lAwsLinkedAccounts.setmAccountOwnerName(lParserModel.getmLinkedAccountName());
                                    lAwsLinkedAccounts.setmTotalCost(lParserModel.getmTotalCost());
                                    mListOfLinkedAccounts.add(lAwsLinkedAccounts);
                                    lLisStringAwsLinkedAccountsHashMap.put(lLinkedAccountName, lAwsLinkedAccounts);
                                } else {
                                    Double lTotal = Double.parseDouble(lAwsLinkedAccounts.getmTotalCost());
                                    lTotal += Double.parseDouble(lParserModel.getmTotalCost());
                                    String ltotal = String.format("%.2f", lTotal);
                                    lAwsLinkedAccounts.setmTotalCost(ltotal);
                                }
                                //Log.i("mListOfLinkedAccounts", "Size = " + mListOfLinkedAccounts.size());
                            }

*/

                            String productCode =  mListOfSortForms.get(lToatalRows[13].replaceAll("\"", ""));
                            if(productCode!=null){
                                lParserModel.setmProductName(productCode);
                            }else {
                                lParserModel.setmProductName(lToatalRows[13].replaceAll("\"", ""));
                            }
                            lParserModel.setmUsageType(lToatalRows[15].replaceAll("\"", ""));
                            lParserModel.setmItemDescription(lToatalRows[18].replaceAll("\"", ""));
                            lParserModel.setmUsageQuantity(lToatalRows[21].replaceAll("\"", ""));
                            lParserModel.setmTotalCost(lToatalRows[28].replaceAll("\"", ""));
//                            lTotalcost += Double.parseDouble(lParserModel.getmTotalCost());
                            lParserModel.setmPayerAccountId(lToatalRows[1].replaceAll("\"", ""));
                            lParserModel.setmBillingPeriodStartDate(lToatalRows[5].replaceAll("\"", ""));
                            lParserModel.setmBillingPeriodEndDate(lToatalRows[6].replaceAll("\"", ""));
                            lParserModel.setmInvoiceDate(lToatalRows[7].replaceAll("\"", ""));
                            if(mAdminName == null){
                                mAdminName         = lToatalRows[8].replaceAll("\"", "");
                            }
                            //Log.i("Payer Account NAme ",lToatalRows[8].replaceAll("\"", ""));
                            lParserModel.setmPayerAccountName(lToatalRows[8].replaceAll("\"", ""));
                            lParserModel.setmLinkedAccountName(lToatalRows[9].replaceAll("\"", ""));
                            lParserModel.setmTaxationAddress(lToatalRows[10].replaceAll("\"", ""));
                            lParserModel.setmSellerOfRecord(lToatalRows[14].replaceAll("\"", ""));
                            lParserModel.setmCurrencyCode(lToatalRows[23].replaceAll("\"", ""));
                            mParserModelList.add(lParserModel);
                            String lLinkedAccountName = lParserModel.getmLinkedAccountName();
                            if ((lParserModel.getmLinkedAccountName() != null) && (!lParserModel.getmLinkedAccountName().isEmpty()) && (lParserModel.getmLinkedAccountName().length() > 0)) {

                                // Log.i("",lParserModel.getmLinkedAccountName());
                                AwsLinkedAccounts lAwsLinkedAccounts = lLisStringAwsLinkedAccountsHashMap.get(lLinkedAccountName);
                                if (lAwsLinkedAccounts == null) {
                                    lAwsLinkedAccounts = new AwsLinkedAccounts();
                                    lAwsLinkedAccounts.setmAccountOwnerName(lParserModel.getmLinkedAccountName());
                                    lAwsLinkedAccounts.setmTotalCost(lParserModel.getmTotalCost());
                                    mListOfLinkedAccounts.add(lAwsLinkedAccounts);
                                    lLisStringAwsLinkedAccountsHashMap.put(lLinkedAccountName, lAwsLinkedAccounts);
                                } else {
                                    Double lTotal = Double.parseDouble(lAwsLinkedAccounts.getmTotalCost());
                                    lTotal += Double.parseDouble(lParserModel.getmTotalCost());
                                    String ltotal = String.format("%.2f", lTotal);
                                    lAwsLinkedAccounts.setmTotalCost(ltotal);
                                }
                                //Log.i("mListOfLinkedAccounts", "Size = " + mListOfLinkedAccounts.size());
                            }
                        } else {
                            mParserTotal.add(lLine);
                            //  Log.i("Total Info : ", lLine);
                        }
                    }
                    if(mParserModelList.size()>0)
                    mReportsOfAllMonths.put(mListOfFiles[j].getAbsolutePath(),mParserModelList);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (lBufferedReader != null) {
                        try {
                            lBufferedReader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Iterator it = mReportsOfAllMonths.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                List<ParserModel> lListParserModels = (List<ParserModel>) pair.getValue();
                //Log.i("size Before", "" + lListParserModels.size());
                Iterator<ParserModel> presentsevicesIterator = lListParserModels.iterator();
                if (presentsevicesIterator != null) {
                    while (presentsevicesIterator.hasNext()) {
                        ParserModel lParserModel = presentsevicesIterator.next();
                        if (!((lParserModel.getmLinkedAccountName().trim()).equals(mLinkedAccounName.trim()))) {
                           // Log.i("LinkedAccountName",mLinkedAccounName+lParserModel.getmLinkedAccountName());
                            presentsevicesIterator.remove();
                        }
                    }
                    Log.i("size After", "" + lListParserModels.size());
                    if(lListParserModels.size() > 0){
                        Log.i("month", "" + pair.getKey());
                    }
                    // UpdateUIForLinkedAccount();
                }
            }
            UpdateUIForLinkedAccount();
            ShowProgressDialog(false);
            mScrollView.fullScroll(ScrollView.FOCUS_UP);
            mScrollView.smoothScrollTo(0, 0);
        }
    }
}