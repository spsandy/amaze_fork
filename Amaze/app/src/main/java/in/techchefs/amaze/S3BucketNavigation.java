package in.techchefs.amaze;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.widget.Toast;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;

import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sandeep on 3/6/15.
 */
public class S3BucketNavigation extends Activity {

    /*   public static String TAG = "in.techchefs.amaze";
       private AmazonS3Client mAmazonS3Client          = null;
       private BasicAWSCredentials mCredentials       = null;
       private String mValidateStatus                 = "NoError";
       public static List<Bucket> mListOfBuckets      = null;
       private ProgressDialog mProgressDialog         = null;
       private HashMap<String,List<String>> mBucketsInfo = null;
       private String mCsvLocation                    = "";
       private Boolean mIsPreveousDownload            = false;*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //   setContentView(R.layout.activity_s3bucketoprations);

       /* SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
        String            lAccess_key        = lSharedPreferences.getString("accesskey", "error");
        String            lSecret_key        = lSharedPreferences.getString("secretkey","error");
        mCredentials                         = new BasicAWSCredentials(lAccess_key,lSecret_key);
        mAmazonS3Client                      = new AmazonS3Client(mCredentials);
        AccessBucket accessBucket            = new AccessBucket();
        MonthReportDownLoad();
        if(mIsPreveousDownload){
            Intent lIntent_dashboard = new Intent(S3BucketNavigation.this,DashBoard.class);
            startActivity(lIntent_dashboard);
            finish();
        }else {
        *//*ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);*//*
            accessBucket.execute();
            ShowProgressDialog(true);
        }
}
    private void MonthReportDownLoad() {

        mCsvLocation  = getFilesDir()+"/";

        File lFile    = new File(mCsvLocation);

        File lListOfFiles []= lFile.listFiles();
        //118558798780-aws-billing-csv-2015-05.csv
        if(lListOfFiles.length > 1){
            mIsPreveousDownload = true;
        }
       *//* FilesInsideBucket lFilesInsideBucket = new FilesInsideBucket();
        lFilesInsideBucket.execute();*//*


    }
    private void ShowProgressDialog(boolean isshow) {
        if(isshow){
            mProgressDialog = new ProgressDialog(S3BucketNavigation.this);
            mProgressDialog.setMessage("please wait....");
            mProgressDialog.show();
        }else{
            mProgressDialog.cancel();
            mProgressDialog = null;
        }
    }
    private void BucketOprations() {
        if(mListOfBuckets.size()==0){
            Intent lIntent_creat_bucket = new Intent(S3BucketNavigation.this,CreateBucket.class);
            startActivity(lIntent_creat_bucket);
            finish();
        }else{
            Intent lIntent_display_bucket = new Intent(S3BucketNavigation.this,DisplayBuckets.class);
            startActivity(lIntent_display_bucket);
            finish();
            ShowProgressDialog(false);
        }
    }
    private void ShowToast(String text) {
             Toast.makeText(S3BucketNavigation.this,text,Toast.LENGTH_LONG).show();
    }
    class AccessBucket extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                 mAmazonS3Client.getS3AccountOwner();
                mListOfBuckets  =  mAmazonS3Client.listBuckets();


            }catch (AmazonS3Exception s3) {
                Log.i("bucket ",s3.getErrorCode());
                mValidateStatus = s3.getErrorCode();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
             if(mValidateStatus.equals("AccessDenied")){
                 ShowProgressDialog(false);
                 SharedPreferences Loginstate    = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
                 SharedPreferences.Editor value  = Loginstate.edit();
                 value.putBoolean("LoginState", false);
                 value.commit();
                 Intent lIntent_login = new Intent(S3BucketNavigation.this,LoginActivity.class);
                 startActivity(lIntent_login);
                 finish();
            }else{
                BucketOprations();
            }
            super.onPostExecute(aVoid);
        }
    }*/
    }
}