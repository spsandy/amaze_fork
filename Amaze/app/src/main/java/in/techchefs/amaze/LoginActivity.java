package in.techchefs.amaze;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;

import java.util.List;
public class LoginActivity extends Activity {
    private EditText mAccess_key= null,mSecret_key=null;
    private Button   mLogin                  = null;
    private ProgressDialog mProgressDialog   = null;
    private String mValidateStatus           = "NoError";
    private BasicAWSCredentials mCredentials = null;
    private AmazonS3Client mAmazonS3Client   = null;
    private List<Bucket> mListOfBuckets      = null;
    private ImageView mHelp                  = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences lsharedPreferences = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
        Boolean loginstate                   = lsharedPreferences.getBoolean("LoginState", false);
        if(!loginstate) {
                setContentView(R.layout.activity_login);
                init();
                mLogin.setOnClickListener(new LoginClickLisner());
                mHelp.setOnClickListener(new LoginClickLisner());
            }
        else {

            Intent lintent_s3_bucket = new Intent(LoginActivity.this,AutoConfiguration.class);
            startActivity(lintent_s3_bucket);
            finish();
            if(mProgressDialog!=null){
                if(mProgressDialog.isShowing()){
                    ShowProgressDialog(false);
                }
            }
        }
   }
    public static boolean isNetworkAvaliable(Context ctx) {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if ((connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED)
                || (connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null && connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .getState() == NetworkInfo.State.CONNECTED)) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    protected void onResume() {
        SharedPreferences lsharedPreferences = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
        Boolean loginstate                   = lsharedPreferences.getBoolean("LoginState", false);
        if(loginstate)
        {

            Intent lintent_s3_bucket = new Intent(LoginActivity.this,AutoConfiguration.class);
            startActivity(lintent_s3_bucket);
            finish();
        }
        super.onResume();
    }
    private void init(){
        mAccess_key      = (EditText) findViewById(R.id.access_key);
        mSecret_key      = (EditText) findViewById(R.id.secret_key);
        mLogin           = (Button) findViewById(R.id.login);
        mHelp            = (ImageView) findViewById(R.id.help);
        mSecret_key.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((keyCode == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    in.hideSoftInputFromWindow(mAccess_key.getApplicationWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    LoginToAmazon(mAccess_key.getText().toString(),mSecret_key.getText().toString());
                    return true;
                }
                return false;
            }});

    }
    private   void LoginToAmazon(String laccess_key, String lsecret_key) {
          if(isNetworkAvaliable(LoginActivity.this)) {
           if ((laccess_key.length() == 20) && (lsecret_key.length() == 40))
             {
                  // BasicAWSCredentials lcredentials = new BasicAWSCredentials(laccess_key, lsecret_key);
                  mCredentials = new BasicAWSCredentials(laccess_key,lsecret_key);
                  //"AKIAJSQJN6KNTUA4DJGA";
                  // @"4mVubjvxbVQSASxEwswspqq1bERk/1LorZglBMNf";
                  //Temp purpose root basheer
                  //mCredentials                 = new BasicAWSCredentials("AKIAIOGWSW2QRO5CXEGQ","7eNCb8ZWzijjNFmXX038F0v+ST1IeNc2TB04kBNX");
                  //Temp purpose root shrilatha
                 //  mCredentials                 = new BasicAWSCredentials("AKIAJSQJN6KNTUA4DJGA","4mVubjvxbVQSASxEwswspqq1bERk/1LorZglBMNf");
                  //Temp purpose iamUSer
                  //mCredentials               = new BasicAWSCredentials("AKIAJSN73YVPSFNN63MA","ku49TF4LEBHt07pMM16BmjyZxW13RRwWdV7+VPQn");
                  mAmazonS3Client = new AmazonS3Client(mCredentials);
                  LoginInBackGround lloginInBackGround = new LoginInBackGround();
                  lloginInBackGround.execute();
                  ShowProgressDialog(true);
              }else {
                  ShowToast("Please Enter Correct Credential");
              }
          }
        else{
            final Dialog dialog = new Dialog(LoginActivity.this);
            dialog.setContentView(R.layout.custom);
            dialog.setTitle("");
            TextView text = (TextView) dialog.findViewById(R.id.text);
            text.setText("The Internet connection appears to be offline Please Check Your Internet Settings");
            Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
            dialogButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }
    }
    private void ShowProgressDialog(boolean isshow) {
        if(isshow){
            mProgressDialog = new ProgressDialog(LoginActivity.this);
            mProgressDialog.setMessage("Verifying Keys...");
            mProgressDialog.show();
        }else{
            if((mProgressDialog!=null)&&(mProgressDialog.isShowing())) {
                mProgressDialog.cancel();
                mProgressDialog = null;
            }
        }
    }
    private void ShowToast(String text) {
        Toast.makeText(LoginActivity.this,text,Toast.LENGTH_LONG).show();
    }
    private void SaveLoginState() {
        SharedPreferences Loginstate    = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
        SharedPreferences.Editor value  = Loginstate.edit();
        value.putString("accesskey", mCredentials.getAWSAccessKeyId());
        value.putString("secretkey", mCredentials.getAWSSecretKey());
        value.putBoolean("LoginState", true);
        value.commit();
        /*Intent lintent_s3_bucket = new Intent(LoginActivity.this,AutoConfiguration.class);
        startActivity(lintent_s3_bucket);
        finish();*/
        Intent addAccountIntent = new Intent(LoginActivity.this,AutoConfiguration.class);
        addAccountIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        addAccountIntent.putExtra(Settings.EXTRA_AUTHORITIES, new String[]{"com.techchefs.amaze"});
        startActivity(addAccountIntent);
    }
    class LoginInBackGround extends AsyncTask<Void,Void,Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                mAmazonS3Client.getS3AccountOwner();
                mListOfBuckets  = mAmazonS3Client.listBuckets();
            }catch (AmazonS3Exception s3) {
                mValidateStatus = s3.getErrorCode();
            }
            catch (Exception e){

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            ShowProgressDialog(false);
            if(mValidateStatus.equals("InvalidAccessKeyId")){
                Toast.makeText(LoginActivity.this,"Please Enter Correct Credential.",Toast.LENGTH_LONG).show();
            }
            else if(mValidateStatus.equals("AccessDenied")){
                ShowToast("You Don't Have Permissions To Access Billling Reports Please Contact Administrator.");
            }
            else if(mListOfBuckets==null){
                ShowToast("Please Enter Correct Credential");
            }
            else{
               //ShowToast("Login successfull");
                SaveLoginState();
            }
            super.onPostExecute(aVoid);
        }
    }
    class LoginClickLisner  implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            if(v.getId()==mLogin.getId()){
                if(mSecret_key.getText().toString().isEmpty()||mAccess_key.getText().toString().isEmpty())
                    Toast.makeText(getApplicationContext(),"Please Enter Login credential",Toast.LENGTH_LONG).show();
                else
                    LoginToAmazon(mAccess_key.getText().toString(), mSecret_key.getText().toString());
            }
            if(v.getId()==mHelp.getId()) {
                //ShowToast("");
                ShowHelpDiaLog("Please Get Your Security Credentials ( Access Key and Secret Key ) From Your IAM Management Console!");
            }
        }
    }
    private void ShowHelpDiaLog(String s) {
        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.custom);
        dialog.setTitle("             Amaze Help");
        dialog.setCancelable(false);
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText(s);
        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
