package in.techchefs.amaze.extras;

import java.util.Comparator;

import in.techchefs.amaze.modles.ParserModel;

/**
 * Created by sandeep on 11/6/15.
 */

public class BillingListSortBasedOnPayerAccountId implements Comparator<ParserModel> {
    @Override
    public int compare(ParserModel lhs, ParserModel rhs) {
        return lhs.getmPayerAccountId().compareToIgnoreCase(rhs.getmPayerAccountId());
    }
}
