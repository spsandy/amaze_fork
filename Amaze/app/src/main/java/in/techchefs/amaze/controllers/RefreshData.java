package in.techchefs.amaze.controllers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.io.IOException;

public class RefreshData extends BroadcastReceiver {
    String mBucketName = "";
    String mFileNAme   = "";
    String mAccessKey  = "";
    String mSecretkey  = "";


    private TransferManager mTransferManager       = null;
    private AmazonS3Client  mAmazonS3Client          = null;
    private BasicAWSCredentials mCredentials       = null;
    @Override
    public void onReceive(final Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.

        Log.d("RefreshData","onReceive");


        SharedPreferences BucketConfig = context.getSharedPreferences("BucketConfig", context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor value = BucketConfig.edit();
        mBucketName     =   BucketConfig.getString("bucketname", "");
        mFileNAme       = BucketConfig.getString("filename", "");
        SharedPreferences Loginstate    = context.getSharedPreferences("Loginstate", context.MODE_MULTI_PROCESS);
        mAccessKey =   Loginstate.getString("accesskey", "");
        mSecretkey =   Loginstate.getString("secretkey", "");

        if((!mBucketName.equals(""))&&(!mFileNAme.equals(""))&&(!mAccessKey.equals(""))&&(!mSecretkey.equals(""))){

            new Thread()
            {
                public void run() {
                    mCredentials         = new BasicAWSCredentials(mAccessKey  , mSecretkey);
                    mAmazonS3Client      = new AmazonS3Client(mCredentials);
                    mTransferManager     = new TransferManager(mCredentials);
                    File file = new File(context.getFilesDir().getAbsolutePath() + "/" + mFileNAme);
                    try {
                        if (!file.exists()) {
                            file.createNewFile();
                            file.setWritable(true);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        mTransferManager.download(mBucketName, mFileNAme, file);
                    } catch (Exception e) {
                        Log.d("",e.toString());
                    }
                    Log.d("Update","Finished");
                }
            }.start();
        }
    }
}
