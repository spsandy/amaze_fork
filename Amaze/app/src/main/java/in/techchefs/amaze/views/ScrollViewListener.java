package in.techchefs.amaze.views;

/**
 * Created by sandeep on 19/6/15.
 */
public interface ScrollViewListener {
    void onScrollChanged(ScrollViewExt scrollView,
                         int x, int y, int oldx, int oldy);
}