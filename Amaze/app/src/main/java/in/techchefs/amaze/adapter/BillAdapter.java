package in.techchefs.amaze.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.techchefs.amaze.BillDetailBasedOnUsageType;
import in.techchefs.amaze.R;
import in.techchefs.amaze.modles.AWSServices;

/**
 * Created by sandeep on 16/6/15.
 */
public class BillAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<AWSServices> mListOfServices;
    private Context mContext = null;
    private List<AWSServices> mListOfUpdatedAwsServices = null;

    public BillAdapter (List<AWSServices> lListOAwsServices,Context context){
        mListOfServices = lListOAwsServices;
        mContext        = context;
        mListOfUpdatedAwsServices = new ArrayList<AWSServices>();
        mListOfUpdatedAwsServices.addAll(mListOfServices);
    }
    @Override
    public int getCount() {
        return mListOfServices.size();
    }
    @Override
    public Object getItem(int position) {
        return mListOfServices.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.list_billdetail, null);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView cost = (TextView) convertView.findViewById(R.id.cost);
        ImageView icon = (ImageView) convertView.findViewById(R.id.arraow);
        name.setText("  " + mListOfServices.get(position).getmNAme());
        Double d = Double.parseDouble(mListOfServices.get(position).getmTotalCost());
        String  ltotal = String.format("%.2f", d);
        cost.setText("$" + ltotal);
        /*

    S3
	EFS
	EBS
	IAM
	CloudWatch
	EC2
	CloudFront
	SNS
	Mobile Analytics
	Route 53
	EMR
	SQS
	Storage Gateway
    Glacier
	Directory Services
 	CloudTrail
    Config
	KMS
    CloudHSM
    EC2 Container
 	Auto Scaling
	Load Balancing
	Lambda
	VPC
	Direct Connect
    WorkSpaces
	WAM
    WorkDocs
	WorkMail
    Kinesis
	Data Pipeline
	Machine Learning
         */


        if(mListOfServices.get(position).getmNAme().equals("S3")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_s3));
            icon.setImageResource(R.drawable.ic_s3);
        }
        else if(mListOfServices.get(position).getmNAme().equals("IAM")){
            //icon.setBackground(mContext.getDrawable(R.drawable.ic_iam));
            icon.setImageResource(R.drawable.ic_iam);
        }
        else if(mListOfServices.get(position).getmNAme().equals("EBS")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_ebs));
            icon.setImageResource(R.drawable.ic_ebs);
        }
        else if(mListOfServices.get(position).getmNAme().equals("EFS")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_efs));

            icon.setImageResource(R.drawable.ic_efs);
        }
        else if(mListOfServices.get(position).getmNAme().equals("CloudWatch")){
          //  icon.setBackground(mContext.getDrawable(R.drawable.ic_cloudwatch));
            icon.setImageResource(R.drawable.ic_cloudwatch);
        }
        else if(mListOfServices.get(position).getmNAme().equals("CloudFront")){
          //  icon.setBackground(mContext.getDrawable(R.drawable.ic_cloudfront));
            icon.setImageResource(R.drawable.ic_cloudfront);
        }
        else if(mListOfServices.get(position).getmNAme().equals("EC2")){
          //  icon.setBackground(mContext.getDrawable(R.drawable.ic_ec2));
            icon.setImageResource(R.drawable.ic_ec2);
        }
        else if(mListOfServices.get(position).getmNAme().equals("SNS")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_sns));
            icon.setImageResource(R.drawable.ic_sns);
        }
        else if(mListOfServices.get(position).getmNAme().equals("Mobile Analytics")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_mobileanlytics));
            icon.setImageResource(R.drawable.ic_mobileanlytics);
        }
        else if(mListOfServices.get(position).getmNAme().equals("Route 53")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_mobileanlytics));
            icon.setImageResource(R.drawable.ic_route53);
        }
        else if(mListOfServices.get(position).getmNAme().equals("EMR")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_emr));
            icon.setImageResource(R.drawable.ic_emr);
        }
        else if(mListOfServices.get(position).getmNAme().equals("SQS")){
          //  icon.setBackground(mContext.getDrawable(R.drawable.ic_sqs));
            icon.setImageResource(R.drawable.ic_sqs);
        }
        else if(mListOfServices.get(position).getmNAme().equals("Storage Gateway")){
           // icon.setBackground(mContext.getDrawable(R.drawable.ic_storagegateway));
            icon.setImageResource(R.drawable.ic_storagegateway);
        }


        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, BillDetailBasedOnUsageType.class);
                intent.putExtra("Total", (mListOfServices.get(position).getmTotalCost()));
                intent.putExtra("name", (mListOfServices.get(position).getmNAme()));
                mContext.startActivity(intent);
            }
        });


        return convertView;
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mListOfServices.clear();
        if (charText.length() == 0) {
            mListOfServices.addAll(mListOfUpdatedAwsServices);
        }
        else
        {
            for (AWSServices wp : mListOfUpdatedAwsServices)
            {
                if (wp.getmNAme().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    mListOfServices.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}
