package in.techchefs.amaze;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;

import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.Bucket;

import java.util.List;

import in.techchefs.amaze.views.ScrollViewExt;
import in.techchefs.amaze.views.ScrollViewListener;


public class CreateBucket extends ActionBarActivity implements ScrollViewListener {

    private Button    mCreatBucket          = null;
    private CheckBox  mBillingCheck         = null;
    private EditText  mBucketName           = null;
    private CheckBox  mEnable               = null;
    private Toolbar   mToolbar              = null;
    private String    mOwnerName            = null;
    private String    mBucketName_copy           = null;
    private AmazonS3Client mAmazonS3Client          = null;
    private BasicAWSCredentials mCredentials       = null;
    private String mValidateStatus                 = "NoError";
    private Dialog mDialog                          = null;
    private EditText mCopy                          = null;
    private Button   mCopyTExt                      = null;
    private Button   mNext                          = null;
    private Button  mNEXTSCroll                     = null;
    private String   mBucketPolicy                  = null;
    private ImageView mScrollImage                  = null;
    private ScrollViewExt mScrollViewExt                 = null;
    private TextView mDone                          = null;
    private TextView mTextViewOfCopy                = null;
    private TextView mTextViewOfCopy1                = null;
    private TextView mTextViewOfCopy2                = null;
    private RelativeLayout mRelative_create_bucket   = null;
    private WebView mWebView                         = null;
    private Button  mWebViewDone                     = null;
    private Button  mConfigDone                      = null;

    private ImageView mCancel                        = null;
    private ImageView mCancelDone                    = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_bucket);
        init();
        ActionBar actionBar =getSupportActionBar();
        actionBar.hide();
        mCreatBucket.setOnClickListener(new ButtonClickLisner());
        mBillingCheck.setOnCheckedChangeListener(new CheckCheckBoxLisner());
    }
    private void init() {
        mCreatBucket  = (Button) findViewById(R.id.create_bucket);
        mBillingCheck = (CheckBox) findViewById(R.id.checkBox_rec);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_create);
      //  mToolbar.inflateMenu(R.menu.menu_bill_detail_based_on_usage_type);


    }
  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      //  mToolbar.inflateMenu(R.menu.menu_bill_detail_based_on_usage_type);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Toast.makeText(CreateBucket.this,"dhdhd"+item.getItemId(),Toast.LENGTH_LONG).show();
        return super.onOptionsItemSelected(item);
    }*/

    @Override
    public void onScrollChanged(ScrollViewExt scrollView, int x, int y, int oldx, int oldy) {
        View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
        int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
        int diff1 = (view.getTop() - (scrollView.getHeight() + scrollView.getScrollY()));
        int diff2 = ( (scrollView.getHeight() + scrollView.getScrollY())-view.getBottom());
        int diff3 = ( (scrollView.getHeight() + scrollView.getScrollY())-view.getTop());
         if (diff == 0) {
           mNEXTSCroll.setText("I Understand");
        }/*if((x==0)&&(y==0)){
          //  mNEXTSCroll.setText("NEXT");
          }*/
    }

    class ButtonClickLisner      implements      View.OnClickListener{
        @Override
        public void onClick(View v) {
            if(mCreatBucket.getId()==v.getId()){
                mDialog = new Dialog(CreateBucket.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.setContentView(R.layout.create_bucket_progress);
                mDialog.setCancelable(false);
                mDialog.show();
                SharedPreferences lSharedPreferences = getSharedPreferences("Loginstate",MODE_MULTI_PROCESS);
                String            lAccess_key        = lSharedPreferences.getString("accesskey", "error");
                String            lSecret_key        = lSharedPreferences.getString("secretkey","error");
                mCredentials                       = new BasicAWSCredentials(lAccess_key,lSecret_key);
                //mCredentials                         = new BasicAWSCredentials("AKIAIOGWSW2QRO5CXEGQ","7eNCb8ZWzijjNFmXX038F0v+ST1IeNc2TB04kBNX");
                mAmazonS3Client                      = new AmazonS3Client(mCredentials);
                CreateBucketAsync accessBucket       = new CreateBucketAsync();
                accessBucket.execute();
            }
        }
    }
    class CheckCheckBoxLisner implements CompoundButton.OnCheckedChangeListener{

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(isChecked) {
                mDialog = new Dialog(CreateBucket.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.setContentView(R.layout.create_bucket_copy_text);
                mCopy                   = (EditText) mDialog.findViewById(R.id.editTexcopy);
                mCopyTExt               = (Button)   mDialog.findViewById(R.id.button);
                mNext                   = (Button)   mDialog.findViewById(R.id.button2);
                mDone                   = (TextView) mDialog.findViewById(R.id.textView11);
                mTextViewOfCopy         = (TextView) mDialog.findViewById(R.id.textView12);
                mTextViewOfCopy1        = (TextView) mDialog.findViewById(R.id.textView13);
                mTextViewOfCopy2        = (TextView) mDialog.findViewById(R.id.textView14);
                mScrollImage            = (ImageView) mDialog.findViewById(R.id.imageView8);
                mNEXTSCroll             = (Button) mDialog.findViewById(R.id.next);
                mCancel                 = (ImageView) mDialog.findViewById(R.id.cancel);
                mRelative_create_bucket = (RelativeLayout) mDialog.findViewById(R.id.relative_create_bucket);
                mRelative_create_bucket.setVisibility(View.INVISIBLE);
                mScrollViewExt          = (ScrollViewExt) mDialog.findViewById(R.id.create_bucket_sc);
                mScrollViewExt.setScrollenable(false);
                mScrollImage.setVisibility(View.INVISIBLE);
                mNEXTSCroll.setVisibility(View.INVISIBLE);
                mCopy.setVisibility(View.INVISIBLE);
                mCopyTExt.setVisibility(View.INVISIBLE);
                mNext.setVisibility(View.INVISIBLE);
                mDone.setVisibility(View.INVISIBLE);
                mTextViewOfCopy.setVisibility(View.INVISIBLE);
                mTextViewOfCopy1.setVisibility(View.INVISIBLE);
                mTextViewOfCopy2.setVisibility(View.INVISIBLE);
                mScrollImage.setVisibility(View.VISIBLE);
                mScrollViewExt.setScrollenable(true);
                mScrollViewExt.setScrollViewListener(CreateBucket.this);
                mNEXTSCroll.setVisibility(View.VISIBLE);
                // mScrollView.setVerticalScrollBarEnabled(true);
                mRelative_create_bucket.setVisibility(View.VISIBLE);
                mDialog.show();
                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.cancel();
                    }
                });

                mNEXTSCroll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (mNEXTSCroll.getText().toString().equals("I Understand")) {
                            mDialog.cancel();
                            mDialog = new Dialog(CreateBucket.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            mDialog.setContentView(R.layout.webview_amaze);
                            mWebView = (WebView) mDialog.findViewById(R.id.mybrowser);
                            mWebViewDone = (Button) mDialog.findViewById(R.id.Done);
                            mWebView.setWebViewClient(new WebViewClient());
                            mWebViewDone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mDialog.cancel();
                                    mDialog = new Dialog(CreateBucket.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                                    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    mDialog.setContentView(R.layout.done);
                                    mCancelDone = (ImageView) mDialog.findViewById(R.id.cancel);
                                    mConfigDone = (Button) mDialog.findViewById(R.id.ok);
                                    mDialog.show();
                                    mCancelDone.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.cancel();
                                        }
                                    });
                                    mConfigDone.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.cancel();
                                        }
                                    });
                                }
                            });
                            mWebView.loadUrl("https://console.aws.amazon.com/billing/home?region=us-east-1#/preferences");
                            mWebView.getSettings().setJavaScriptEnabled(true);
                            mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                            mDialog.show();
                        } else {
                            mScrollViewExt.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    }
                });


            }

        }
    }
    private void  ShowToast(String ltext){
        Toast.makeText(CreateBucket.this,ltext,Toast.LENGTH_LONG).show();
    }
    class  CreateBucketAsync extends AsyncTask<Void ,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
           try {
                mOwnerName = mAmazonS3Client.getS3AccountOwner().getDisplayName();
                mBucketName_copy = ("Amaze-" + mOwnerName + "-" + System.currentTimeMillis()).toLowerCase();
                mBucketPolicy    = "{\"Version\":\"2008-10-17\",\"Id\":\"Policy1335892530063\",\"Statement\":[{\"Sid\":\"Stmt1335892150622\",\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"arn:aws:iam::386209384616:root\"},\"Action\":[\"s3:GetBucketAcl\",\"s3:GetBucketPolicy\"],\"Resource\":\"arn:aws:s3:::"+mBucketName_copy+"\"},{\"Sid\":\"Stmt1335892526596\",\"Effect\":\"Allow\",\"Principal\":{\"AWS\":\"arn:aws:iam::386209384616:root\"},\"Action\":\"s3:PutObject\",\"Resource\":\"arn:aws:s3:::"+mBucketName_copy+"/*\"}]}\n";
                mAmazonS3Client.createBucket(mBucketName_copy);
                mAmazonS3Client.setBucketPolicy(mBucketName_copy, mBucketPolicy);
            }catch (Exception e) {
               mValidateStatus = "error";
           }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            mCreatBucket.setClickable(false);
            mCreatBucket.setText("Created");
            mCreatBucket.setBackgroundColor(Color.parseColor("#79969560"));
            mDialog.cancel();
            if(mValidateStatus.equals("error")) {
                ShowToast("Error occured please try again!");
            }
            else {
                //ShowToast("Successfully created bucket for you");
                mDialog = new Dialog(CreateBucket.this,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mDialog.setContentView(R.layout.create_bucket_copy_text);
                mCopy                   = (EditText) mDialog.findViewById(R.id.editTexcopy);
                mCopyTExt               = (Button)   mDialog.findViewById(R.id.button);
                mNext                   = (Button)   mDialog.findViewById(R.id.button2);
                mDone                   = (TextView) mDialog.findViewById(R.id.textView11);
                mTextViewOfCopy         = (TextView) mDialog.findViewById(R.id.textView12);
                mTextViewOfCopy1        = (TextView) mDialog.findViewById(R.id.textView13);
                mTextViewOfCopy2        = (TextView) mDialog.findViewById(R.id.textView14);
                mScrollImage            = (ImageView) mDialog.findViewById(R.id.imageView8);
                mNEXTSCroll             = (Button) mDialog.findViewById(R.id.next);
                mCancel                 = (ImageView) mDialog.findViewById(R.id.cancel);
                mRelative_create_bucket = (RelativeLayout) mDialog.findViewById(R.id.relative_create_bucket);
                mRelative_create_bucket.setVisibility(View.INVISIBLE);
                mScrollViewExt          = (ScrollViewExt) mDialog.findViewById(R.id.create_bucket_sc);
                mScrollViewExt.setScrollenable(false);
                mScrollImage.setVisibility(View.INVISIBLE);
                mNEXTSCroll.setVisibility(View.INVISIBLE);
                mDialog.setCancelable(true);
                mCopy.setText(mBucketName_copy);
                mDialog.show();
                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.cancel();
                    }
                });
                mNext.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // ShowToast("Move To next");
                        mCopy.setVisibility(View.INVISIBLE);
                        mCopyTExt.setVisibility(View.INVISIBLE);
                        mNext.setVisibility(View.INVISIBLE);
                        mDone.setVisibility(View.INVISIBLE);
                        mTextViewOfCopy.setVisibility(View.INVISIBLE);
                        mTextViewOfCopy1.setVisibility(View.INVISIBLE);
                        mTextViewOfCopy2.setVisibility(View.INVISIBLE);
                        mScrollImage.setVisibility(View.VISIBLE);
                        mScrollViewExt.setScrollenable(true);
                        mScrollViewExt.setScrollViewListener(CreateBucket.this);
                        mNEXTSCroll.setVisibility(View.VISIBLE);
                        // mScrollView.setVerticalScrollBarEnabled(true);
                        mRelative_create_bucket.setVisibility(View.VISIBLE);
                        mNEXTSCroll.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                if (mNEXTSCroll.getText().toString().equals("I Understand")) {
                                    mDialog.cancel();
                                    mDialog = new Dialog(CreateBucket.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                                    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                    mDialog.setContentView(R.layout.webview_amaze);
                                    mWebView = (WebView) mDialog.findViewById(R.id.mybrowser);
                                    mWebViewDone = (Button) mDialog.findViewById(R.id.Done);
                                    mWebView.setWebViewClient(new WebViewClient());
                                    mWebViewDone.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mDialog.cancel();
                                            mDialog = new Dialog(CreateBucket.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                                            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                            mDialog.setContentView(R.layout.done);
                                            mConfigDone = (Button) mDialog.findViewById(R.id.ok);

                                            mDialog.show();
                                            mCancelDone = (ImageView) mDialog.findViewById(R.id.cancel);
                                            mCancelDone.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    mDialog.cancel();
                                                }
                                            });
                                            mConfigDone.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    mDialog.cancel();
                                                }
                                            });
                                        }
                                    });
                                    mWebView.loadUrl("https://console.aws.amazon.com/billing/home?region=us-east-1#/preferences");
                                    mWebView.getSettings().setJavaScriptEnabled(true);
                                    mWebView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                                    mDialog.show();
                                } else {
                                    mScrollViewExt.fullScroll(ScrollView.FOCUS_DOWN);
                                }
                            }
                        });

                    }
                });
                mCopyTExt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("bucket", mBucketName_copy);
                        clipboard.setPrimaryClip(clip);
                        mCopyTExt.setText("Copied");
                    }
                });


            }
        }
    }
}
