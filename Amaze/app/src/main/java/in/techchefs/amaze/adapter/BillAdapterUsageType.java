package in.techchefs.amaze.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.Inflater;

import in.techchefs.amaze.BillDetailBasedOnUsageType;
import in.techchefs.amaze.R;
import in.techchefs.amaze.modles.AWSServices;
import in.techchefs.amaze.modles.UsageDecription;

/**
 * Created by sandeep on 17/6/15.
 */
public class BillAdapterUsageType extends BaseAdapter {


    private  LayoutInflater mInflater = null;
    private Context   mContext  = null;
    private HashMap<String ,UsageDecription>mMapOfUsageType = null;
    private List<UsageDecription> mListOfUsageType = null;
    private List<UsageDecription> mListOfUpdatedAwsServices = null;

    public BillAdapterUsageType(Context lcontext ,HashMap<String ,UsageDecription>lMapOfUsageType ){
        mContext         =   lcontext;
        mMapOfUsageType  =   lMapOfUsageType;
        mListOfUsageType =   new ArrayList<UsageDecription>();
        mListOfUpdatedAwsServices = new ArrayList<UsageDecription>();

        setListOFUsageType();

    }

    private void setListOFUsageType() {
        UsageDecription usageDecription = new UsageDecription();
        usageDecription.setmInvoiceDate(BillDetailBasedOnUsageType.mInvoiceDate);
        usageDecription.setmPayer(BillDetailBasedOnUsageType.mPayerName);
        usageDecription.setmBillingPeroid(BillDetailBasedOnUsageType.mStratDate + " - "+BillDetailBasedOnUsageType.mEndDate);
        usageDecription.setmCurrency(BillDetailBasedOnUsageType.mCurrency);
        usageDecription.setmUsageType("h");
        mListOfUsageType.add(usageDecription);
        Iterator it = mMapOfUsageType.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            mListOfUsageType.add((UsageDecription) pair.getValue());
       }
        mListOfUpdatedAwsServices.addAll(mListOfUsageType);
    }

    @Override
    public int getCount() {
        return mListOfUsageType.size();
    }

    @Override
    public Object getItem(int position) {
        return mListOfUsageType.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (mInflater == null)
            mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = mInflater.inflate(R.layout.usagetype_list_itmes, null);

        TextView lDecription1 = (TextView) convertView.findViewById(R.id.usage_text_row1_col1);
        TextView lDecription2 = (TextView) convertView.findViewById(R.id.usage_text_row1_col2);
        TextView lDecription3 = (TextView) convertView.findViewById(R.id.usage_text_row1_col3);
        TextView lDecription4 = (TextView) convertView.findViewById(R.id.usage_text_row1_col4);
        lDecription1.setText(mListOfUsageType.get(position).getmUsageType());
        lDecription2.setText(mListOfUsageType.get(position).getDescription());
        lDecription3.setText(mListOfUsageType.get(position).getmQuatity()+"");
        lDecription4.setText(mListOfUsageType.get(position).getmTotalCost() + "");

        TableRow tableRow1    = (TableRow) convertView.findViewById(R.id.row1);

        TableRow tableRow2    = (TableRow) convertView.findViewById(R.id.row2);

        TableRow tableRow3    = (TableRow) convertView.findViewById(R.id.row3);

        TableRow tableRow4    = (TableRow) convertView.findViewById(R.id.row4);
        if((position==0)){
            TextView textView1 = (TextView) convertView.findViewById(R.id.usage_text_row1);
            TextView textView2 = (TextView) convertView.findViewById(R.id.usage_text_row2);
            TextView textView3 = (TextView) convertView.findViewById(R.id.usage_text_row3);
            TextView textView4 = (TextView) convertView.findViewById(R.id.usage_text_row4);
            textView1.setText("Invoice Date");
            textView2.setText("Payer");
            textView3.setText("Billing Perioud   ");
            textView4.setText("Currency");
            lDecription1.setText(BillDetailBasedOnUsageType.mInvoiceDate);
            lDecription2.setText(BillDetailBasedOnUsageType.mPayerName);
            lDecription3.setText(BillDetailBasedOnUsageType.mStratDate+" - "+BillDetailBasedOnUsageType.mEndDate);
            lDecription4.setText(BillDetailBasedOnUsageType.mCurrency);
            tableRow1.setBackgroundColor(Color.parseColor("#CC3E00"));
            tableRow2.setBackgroundColor(Color.parseColor("#CC3E00"));
            tableRow3.setBackgroundColor(Color.parseColor("#CC3E00"));
            tableRow4.setBackgroundColor(Color.parseColor("#CC3E00"));
        }
        else if(position%2==0){
            tableRow1.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
            tableRow2.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
            tableRow3.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
            tableRow4.setBackgroundColor(mContext.getResources().getColor(android.R.color.darker_gray));
        }
        /*name.setText("  " + mListOfServices.get(position).getmNAme());
        Double d = Double.parseDouble(mListOfServices.get(position).getmTotalCost());
        String  ltotal = String.format("%.2f", d);
        cost.setText("$" + ltotal);*/

        /*convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, BillDetailBasedOnUsageType.class);
                intent.putExtra("Total", (mListOfServices.get(position).getmTotalCost()));
                intent.putExtra("name", (mListOfServices.get(position).getmNAme()));
                mContext.startActivity(intent);
            }
        });*/
        return convertView;
    }
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        mListOfUsageType.clear();
        if (charText.length() == 0) {
            mListOfUsageType.addAll(mListOfUpdatedAwsServices);
        }
        else
        {
            UsageDecription usageDecription = new UsageDecription();
            usageDecription.setmInvoiceDate(BillDetailBasedOnUsageType.mInvoiceDate);
            usageDecription.setmPayer(BillDetailBasedOnUsageType.mPayerName);
            usageDecription.setmBillingPeroid(BillDetailBasedOnUsageType.mStratDate + " - "+BillDetailBasedOnUsageType.mEndDate);
            usageDecription.setmCurrency(BillDetailBasedOnUsageType.mCurrency);
            usageDecription.setmUsageType("hh");
            mListOfUsageType.add(usageDecription);
            for (UsageDecription wp : mListOfUpdatedAwsServices) {
              if (wp.getmUsageType().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    mListOfUsageType.add(wp);
                    //Log.i("usage type",charText);
                   // Log.i("wp usage type", wp.getmUsageType() + "size = "+mListOfUsageType.size());
                }
            }
        }
        notifyDataSetChanged();
    }

}
