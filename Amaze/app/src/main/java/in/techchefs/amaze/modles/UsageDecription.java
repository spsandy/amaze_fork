package in.techchefs.amaze.modles;

/**
 * Created by sandeep on 17/6/15.
 */
public class UsageDecription {

    private Double mTotalCost;
    private String mDescription;
    private String mLinkedAcconuName;
    private Double mQuatity;

    private String mInvoiceDate;

    public String getmInvoiceDate() {
        return mInvoiceDate;
    }

    public void setmInvoiceDate(String mInvoiceDate) {
        this.mInvoiceDate = mInvoiceDate;
    }

    public String getmPayer() {
        return mPayer;
    }

    public void setmPayer(String mPayer) {
        this.mPayer = mPayer;
    }

    public String getmBillingPeroid() {
        return mBillingPeroid;
    }

    public void setmBillingPeroid(String mBillingPeroid) {
        this.mBillingPeroid = mBillingPeroid;
    }

    public String getmCurrency() {
        return mCurrency;
    }

    public void setmCurrency(String mCurrency) {
        this.mCurrency = mCurrency;
    }

    private String mPayer;
    private String mBillingPeroid;
    private String mCurrency;


    public String getmUsageType() {
        return mUsageType;
    }

    public void setmUsageType(String mUsageType) {
        this.mUsageType = mUsageType;
    }

    private String mUsageType;

    public Double getmQuatity() {
        return mQuatity;
    }

    public void setmQuatity(Double mQuatity) {
        this.mQuatity = mQuatity;
    }

    public String getmLinkedAcconuName() {
        return mLinkedAcconuName;
    }

    public void setmLinkedAcconuName(String mLinkedAcconuName) {
        this.mLinkedAcconuName = mLinkedAcconuName;
    }


    public Double getmTotalCost() {
        return mTotalCost;
    }

    public void setmTotalCost(Double mTotalCost) {
        this.mTotalCost = mTotalCost;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }


}
