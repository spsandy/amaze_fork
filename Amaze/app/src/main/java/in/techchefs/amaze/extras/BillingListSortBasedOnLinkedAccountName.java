package in.techchefs.amaze.extras;

import java.util.Comparator;

import in.techchefs.amaze.modles.ParserModel;

/**
 * Created by sandeep on 11/6/15.
 */
public class BillingListSortBasedOnLinkedAccountName implements Comparator<ParserModel> {
    @Override
    public int compare(ParserModel lhs, ParserModel rhs) {
        return lhs.getmPayerAccountName().compareToIgnoreCase(rhs.getmPayerAccountName());
    }
}

